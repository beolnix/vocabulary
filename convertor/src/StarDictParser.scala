import java.io.{File, PrintWriter}

object StarDictParser extends App {
  val fileLines = io.Source.fromFile("./Macmillan English Thesaurus.xml").getLines.toList
  var word: String = null
  var offset: Integer = 0
  var desc: String = null
  var count = 0;
  val file = new File("./out.txt")
  if (!file.exists())
    file.createNewFile()
  val out = new PrintWriter( file , "UTF-8")
  for (line <- fileLines) {
    if (line.startsWith("<blockquote><b>")) {
      word = line.replace("<blockquote><b>", "")
      offset = word.indexOf("<")
      word = word.substring(0, offset)
    }

    if (line.startsWith("<blockquote><blockquote><blockquote>") && word != null) {
      desc = line.replace("<blockquote><blockquote><blockquote>", "")
        .replace("</blockquote></blockquote></blockquote>", "")
        .replace("]]>", "")
        .replace("<i>", "")
        .replace("</i>", "")
        .replace("<abr>", "")
        .replace("</abr>", ": ")
        .replace("<c c=\"rosybrown\">", "")
        .replace("</c>", ": ")
        .replace("<c c=\"darkgreen\">", "")
        .replace("<c c=\"darkslategray\">", "")
        .replace("<kref>", "")
        .replace("</kref>", "")
        .replace("<b>", "")
        .replace("</b>", "")
        .replace("<sup>", "")
        .replace("</sup>", "")
        .trim()
      if (desc.endsWith("1"))
        desc = desc.substring(0, desc.length - 1).trim()

      if (desc.endsWith("1"))
        desc = desc.substring(0, desc.length - 1).trim()

      out.append(word + " -- " + desc + "\n")
    }


  }
  out.flush()
  out.close()
}