import play.Project._
import sbt._, Keys._

object ApplicationBuild extends Build {

  import BuildSettings._
  import Dependencies._

  override def rootProject = Some(vocabulary)

  lazy val vocabulary = _root_.play.Project("vocabulary", "1.0") settings (
    offline := true,
    libraryDependencies ++= Seq(
      scalaz, scalalib, hasher, config, apache, scalaTime,
      csv, jgit, actuarius, scalastic, findbugs, RM,
      PRM, spray.caching, cache),
    scalacOptions := compilerOptions,
    resolvers := Dependencies.Resolvers.commons,
    sources in doc in Compile := List()
    )


  lazy val modules = Seq()

  lazy val moduleRefs = modules map projectToRef
  lazy val moduleCPDeps = moduleRefs map { new sbt.ClasspathDependency(_, None) }

}
