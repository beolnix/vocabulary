package security

import play.api.test.{PlaySpecification, FakeRequest, WithApplication}
import utils.{SecurityTestUtils, Helper}
import scala.concurrent.Await
import play.api.mvc.SimpleResult
import org.specs2.execute.AsResult
import scala.concurrent.duration.Duration
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class LoginControllerTestCase extends PlaySpecification with SecurityTestUtils {
  "Login Controller" should {

    "authorize test user successfully" in new WithApplication {
      withUser { user =>
        val Some(futureResult) = route(FakeRequest(GET, "/restLogin?login=" + Helper.encodeURL(user.login) +
          "&pswd=" + Helper.encodeURL(user.pwdHash)))

        val result = Await.result[SimpleResult](futureResult, Duration.Inf)

        result.header.status must be equalTo 200

      }
    }

  }

}
