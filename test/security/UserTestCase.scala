package security

import org.specs2.runner._
import org.junit.runner._
import play.api.test.{FakeApplication, PlaySpecification, WithApplication}
import scala.concurrent.{ExecutionContext, Await}
import ExecutionContext.Implicits.global
import org.specs2.execute.AsResult
import security.domain.model.User
import utils.SecurityTestUtils

@RunWith(classOf[JUnitRunner])
class UserTestCase extends PlaySpecification with SecurityTestUtils {

  "new user" should {

    "be created successfully" in new WithApplication {
      val user = createTestUser()

      Some(user) must beSome[User]
      user.id must beSome[String]

      // clean test data
      val userId = deleteUser(user.id.get)
      Some(userId) must beSome[String]
    }

  }

}
