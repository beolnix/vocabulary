package utils

import security.domain.model.User
import security.domain.dao.UserJsonDAO
import scala.concurrent.duration.Duration
import scala.compat.Platform
import security.domain.services.UserService
import scala.concurrent.{ExecutionContext, Await}
import ExecutionContext.Implicits.global
import play.modules.reactivemongo.ReactiveMongoPlugin

trait SecurityTestUtils {

  def withUser(f: (User) => Unit) {
    val user = createTestUser()
    f(user)
    deleteUser(user.id.get)
  }

  def deleteUser(id: String): String = {
    val futureDelete = UserJsonDAO.delete(id)
    val userId = Await.result[String](futureDelete, Duration.Inf)

    userId
  }

  def createTestUser(): User = {
    val time = Platform.currentTime

    val login = "test-" + time
    val password = "test"
    val futureUser = UserService.createUser(login, password)
    val user = Await.result[User](futureUser, Duration.Inf)

    user
  }

}
