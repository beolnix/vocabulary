package utils

object Helper {
  def encodeURL(url: String): String = {
    java.net.URLEncoder.encode(url, "UTF-8")
  }
}
