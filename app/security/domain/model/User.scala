package security.domain.model

import play.api.libs.json.{JsObject, JsValue}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import common.domain.model.BaseModel

case class User( var login: String = null,
                 var pwdHash: String = null,
                 var id: Option[String] = None
                 )


object User {
  val LOGIN_PROP = "login"
  val PWD_HASH_PROP = "pwdHash"

  import play.api.libs.json.Json

  implicit val userFormat = Json.format[User]

  implicit def readValue(json: JsValue): User = {
    userFormat.reads(json.as[JsObject]).get
  }

  implicit def readObject(json: JsObject): User = {
    userFormat.reads(json).get
  }

  implicit def writeValue(model: User): JsValue = {
    userFormat.writes(model)
  }

  implicit def writeObject(model: User): JsObject = {
    userFormat.writes(model).as[JsObject]
  }

  implicit def readOptionObject(jsonOpt: Option[JsObject]): Option[User] = {
    if (jsonOpt.isDefined) {
      Option(jsonOpt.get)
    } else {
      None
    }
  }

  implicit def readFutureOption(jsonFutureOpt: Future[Option[JsObject]]): Future[Option[User]] = {
    jsonFutureOpt.map {json => json}
  }

  implicit def readFuture(jsonFuture: Future[JsObject]): Future[User] = {
    jsonFuture.map { json => json }
  }
}