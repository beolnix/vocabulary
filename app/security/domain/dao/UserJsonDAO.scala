package security.domain.dao

import play.modules.reactivemongo.json.collection.JSONCollection
import scala.concurrent.{ExecutionContext, Future}
import security.domain.model.User
import play.api.libs.json.JsObject
import ExecutionContext.Implicits.global
import common.domain.dao.BaseJsonDAO

object UserJsonDAO extends BaseJsonDAO {

  val collection = db.collection[JSONCollection]("users")

  def findUserByLoginAndPasswordHash(login: String, pwdHash: String): Future[Option[JsObject]] = {
    collection.find(User(login, pwdHash)).cursor[JsObject].headOption
  }

}
