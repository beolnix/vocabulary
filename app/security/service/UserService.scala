package security.service

import security.domain.model.User
import play.api.cache.Cache
import scala.concurrent.{ExecutionContext, Future}
import security.domain.dao.UserJsonDAO
import common.service.MyAppConfig
import play.api.Play.current
import ExecutionContext.Implicits.global


object UserService {

  def login(login: String, password: String): Future[Option[(User, String)]] = {
    val futureUser = UserJsonDAO.findUserByLoginAndPasswordHash(login, password)
    futureUser.map { userJs =>
      if (userJs.isDefined) {
        val user = User.readObject(userJs.get)
        val uuid = user.id.get
        Cache.set(uuid, user, MyAppConfig.cacheDuration)
        Some(user, uuid)
      } else {
        None
      }
    }
  }

  def fakeLogin(): Future[Option[(User, String)]] = {
    val testUserOrigin = MyAppConfig.testUser
    login(testUserOrigin.login, testUserOrigin.pwdHash).flatMap { resultOpt =>
      if (resultOpt.isDefined) {
        Future.successful(resultOpt)
      } else {
        UserJsonDAO.save(testUserOrigin).map { newUser =>
          val user = User.readObject(newUser)
          val uuid = user.id.get
          Cache.set(uuid, user, MyAppConfig.cacheDuration)
          Some(user, uuid)
        }
      }
    }
  }

  def createUser(login: String, pwdHash: String): Future[User] = {
    val user = User(login, pwdHash)
    UserJsonDAO.save(user)
  }
}
