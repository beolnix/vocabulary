package security.web

import play.api.mvc._


import scala.concurrent.{Future, ExecutionContext}

import ExecutionContext.Implicits.global
import security.service.UserService
import common.service.MyAppConfig

object LoginController extends Controller {

  def restLogin(login: String, pswd: String) = Action.async {
    implicit request =>
      UserService.login(login, pswd).map {
        userOpt =>
          if (userOpt.isDefined) {
            val (user, uuid) = userOpt.get
            Ok withSession session + ("uuid" -> uuid)
          } else {
            Unauthorized
          }
      }
  }

}