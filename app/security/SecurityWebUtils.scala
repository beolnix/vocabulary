package security

import play.api.mvc.{Session, SimpleResult, Request}
import play.api.mvc.Results._
import play.api.cache.Cache
import security.domain.model.User
import play.api.Play.current
import assets.js.test.layout.BrowserDetection
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import common.service.MyAppConfig
import security.service.UserService
import security.domain.dao.UserJsonDAO


trait SecurityWebUtils extends BrowserDetection {
  def authenticated[A](f: (User) => Future[SimpleResult])(implicit session: Session, request: Request[A]): Future[SimpleResult] = {

    val uuid = request.session.get("uuid")
    if (uuid.isDefined) {
      val user = Cache.get(uuid.get)
      if (user.isDefined) {
        val trueUser = user.get.asInstanceOf[User]
        return f(trueUser)
      } else {
        return UserJsonDAO.findById(uuid.get) flatMap {
          userJs =>
            if (userJs.isDefined) {
              val databaseUser = User.readObject(userJs.get)
              Cache.set(databaseUser.id.get, databaseUser, MyAppConfig.cacheDuration)
              f(databaseUser)
            } else {
              Future.successful(Ok(views.html.full.vocabulary(false)))
            }
        }
      }
    }

    // if there is no open session
    if (MyAppConfig.isTestEnv) {
      UserService.fakeLogin().flatMap {
        loginResult =>
          val (newTestUser, uuid) = loginResult.get
          f(newTestUser) map {
            result => result withSession session + ("uuid" -> uuid)
          }
      }
    } else {
      Future.successful(Ok(views.html.full.vocabulary(false)))
    }
  }

}
