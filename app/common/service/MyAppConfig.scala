package common.service

import play.api.Play._
import scala.Some
import reactivemongo.api.MongoDriver
import play.modules.reactivemongo.ReactiveMongoPlugin
import scala.collection.JavaConversions._

import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global
import security.domain.model.User
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit

object MyAppConfig {

  val TEST_SERVERS_PARAM = "test.mongodb.servers"
  val TEST_DB_NAME = "test.mongodb.db"

  val ENV_PARAM = "env"
  val TEST_ENV_NAME = "test"

  val TEST_USERNAME_PARAM = "test.login"
  val TEST_PASSWORD_PARAM = "test.password"

  val isTestEnv: Boolean = current.configuration.getString(ENV_PARAM) == Some(TEST_ENV_NAME)
  val testServers = current.configuration.getStringList(TEST_SERVERS_PARAM).get
  val testDBName = current.configuration.getString(TEST_DB_NAME).get
  val cacheDuration = Duration(30, TimeUnit.MINUTES)

  val appDatabase = {
    if (isTestEnv) {
      val driver = new MongoDriver
      val connection = driver.connection(testServers)
      connection(MyAppConfig.testDBName)
    } else {
      ReactiveMongoPlugin.db
    }
  }

  val testUser = {
    val username = current.configuration.getString(TEST_USERNAME_PARAM)
    val pwd = current.configuration.getString(TEST_PASSWORD_PARAM)
    User(username.get, pwd.get)
  }

}
