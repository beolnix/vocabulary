package common.domain.model

import play.api.libs.json.{JsObject, JsValue, Json}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global

class BaseModel

abstract class BaseModelFormatter [M <: BaseModel] {
  implicit val modelFormat: play.api.libs.json.Format[M]

  implicit def readValue(json: JsValue): M = {
    modelFormat.reads(json.as[JsObject]).get
  }

  implicit def readObject(json: JsObject): M = {
    modelFormat.reads(json).get
  }

  implicit def writeValue(model: M): JsValue = {
    modelFormat.writes(model)
  }

  implicit def writeObject(model: M): JsObject = {
    modelFormat.writes(model).as[JsObject]
  }

  implicit def readOptionObject(jsonOpt: Option[JsObject]): Option[JsObject] = {
    if (jsonOpt.isDefined) {
      Option(jsonOpt.get)
    } else {
      None
    }
  }

  implicit def readFutureOption(jsonFutureOpt: Future[Option[JsObject]]): Future[Option[JsObject]] = {
    jsonFutureOpt.map {json => json}
  }

  implicit def readFuture(jsonFuture: Future[JsObject]): Future[JsObject] = {
    jsonFuture.map { json => json }
  }
}

object BaseModel {
  val ID_PROP = "id"
  val USER_ID_PROP = "userId"
  val NAME_PROP = "name"

}


