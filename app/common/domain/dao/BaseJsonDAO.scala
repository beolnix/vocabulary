package common.domain.dao

import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._

import ExecutionContext.Implicits.global
import reactivemongo.bson.BSONObjectID

import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import common.service.MyAppConfig
import common.domain.model.{BaseModel}


trait BaseJsonDAO {

  protected val db = MyAppConfig.appDatabase

  def collection: JSONCollection

  def findByIdAndUserId(userId: String, itemId: String): Future[Option[JsObject]] = {
    val selector = Json.obj(BaseModel.ID_PROP -> itemId, BaseModel.USER_ID_PROP -> userId)
    collection.find(selector).cursor[JsObject].headOption
  }

  def findById(id: String): Future[Option[JsObject]] = {
    val selector = Json.obj(BaseModel.ID_PROP -> id)
    val cursor = collection.find(selector).cursor[JsObject]
    cursor.headOption
  }

  def findByUserIdAndCommon(userId: String): Future[List[JsObject]] = {
    val selector = Json.obj("$OR" -> Json.obj(BaseModel.USER_ID_PROP -> userId, BaseModel.USER_ID_PROP -> JsNull))
    collection.find(selector).cursor[JsObject].collect[List]()
  }

  def findByNameAndUserId(userId: String, name: String): Future[Option[JsObject]] = {
    val selector = Json.obj("$OR" -> Json.obj(BaseModel.USER_ID_PROP -> userId, BaseModel.USER_ID_PROP -> JsNull),
                            BaseModel.NAME_PROP -> name)
    collection.find(selector).cursor[JsObject].headOption
  }

  def findByNameAndSpecificUserId(userId: String, name: String): Future[Option[JsObject]] = {
    val selector = Json.obj(BaseModel.USER_ID_PROP -> userId,
                            BaseModel.NAME_PROP -> name)
    collection.find(selector).cursor[JsObject].headOption
  }

  def findByName(name: String): Future[Option[JsObject]] = {
    val selector = Json.obj(BaseModel.USER_ID_PROP -> JsNull,
      BaseModel.NAME_PROP -> name)
    collection.find(selector).cursor[JsObject].headOption
  }

  def findByUserId(userId: String): Future[List[JsObject]] = {
    val selector = Json.obj(BaseModel.USER_ID_PROP -> userId)
    collection.find(selector).cursor[JsObject].collect[List]()
  }

  def save(item: JsObject): Future[JsObject] = {
    if ((item \ BaseModel.ID_PROP).asOpt[String].isDefined)
      update(item)
    else
      create(item)
  }

  private def update(item: JsObject): Future[JsObject] = {
    val bookWithOutId = item - BaseModel.ID_PROP
    val modifier = Json.obj("$set" -> bookWithOutId)
    val selector = Json.obj(BaseModel.ID_PROP -> item \ BaseModel.ID_PROP)
    collection.update(selector, modifier).map {
      _ => item
    }
  }

  private def create(item: JsObject): Future[JsObject] = {
    val bookWithId = item + (BaseModel.ID_PROP -> JsString(BSONObjectID.generate.stringify))
    collection.insert(bookWithId).map {
      _ => bookWithId
    }
  }

  def delete(itemId: String): Future[String] = {
    val selector = Json.obj(BaseModel.ID_PROP -> itemId)
    collection.remove(selector).map {
      _ => itemId
    }
  }

}
