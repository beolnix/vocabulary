package vocabulary.domain.model

import play.api.libs.json.{Format, JsObject, JsValue}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import common.domain.model.{BaseModel, BaseModelFormatter}


case class ValidationError (field: String, desc: String, errorType: String) extends BaseModel

object ValidationError extends BaseModelFormatter[ValidationError] {
  val FIELD_PROP = "field"
  val DESC_PROP = "desc"
  val ERROR_TYPE_PROP = "errorType"

  import play.api.libs.json.Json

  override implicit val modelFormat: Format[ValidationError] = Json.format[ValidationError]

}
