package vocabulary.domain.model

import common.domain.model.{BaseModelFormatter, BaseModel}
import play.api.libs.json.{Json, Format}

case class Reference(var id: Option[String],
                     var name: String,
                     var refType: String) extends BaseModel

object Reference extends BaseModelFormatter[Reference] {

  implicit override val modelFormat: Format[Reference] = Json.format[Reference]

  val REF_TYPE_PROP = "refType"

}