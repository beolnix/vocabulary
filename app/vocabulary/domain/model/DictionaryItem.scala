package vocabulary.domain.model

import play.api.libs.json.{Json, Format, JsObject, JsValue}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import common.domain.model.{BaseModelFormatter, BaseModel}

case class DictionaryItem ( var id: Option[String],
                            var dictionaryId: String,
                            var key: ItemContent,
                            var value: ItemContent ) extends BaseModel

object DictionaryItem extends BaseModelFormatter[DictionaryItem] {
  val DICTIONARY_ID_PROP = "dictionaryId"
  val KEY_PROP = "key"
  val VALUE_PROP = "value"

  implicit override val modelFormat: Format[DictionaryItem] = Json.format[DictionaryItem]
  
}
