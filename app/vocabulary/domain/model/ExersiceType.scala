package vocabulary.domain.model

object ExersiceType {
  val ShowFrontHideBack = "showFrontHideBack"
  val ShowBackHideFront = "showBackHideFront"
  val ShowDescHideBack = "showDescHideBack"
  val ShowDescHideFront = "showDescHideFront"
  val ShowBackTypeFront = "showBackTypeFront"
}
