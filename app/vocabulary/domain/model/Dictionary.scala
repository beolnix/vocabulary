package vocabulary.domain.model

import play.api.libs.json.{Json, Format, JsObject, JsValue}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import common.domain.model.{BaseModel, BaseModelFormatter}


case class Dictionary (var id: Option[String],
                       var userId: Option[String],
                       var name: String,
                       var desc: String,
                       var fieldName: String,
                       var srcLangCode: String,
                       var dstLangCode: String,
                       var contentType: String,
                       var public: Boolean = false) extends BaseModel

object Dictionary extends BaseModelFormatter[Dictionary] {
  val DESC_PROP = "desc"
  val FIELD_NAME_PROP = "fieldName"
  val SRC_LANG_CODE_PROP = "srcLangCode"
  val DST_LANG_CODE_PROP = "dstLangCode"
  val CONTENT_TYPE_PROP = "contentType"
  val PUBLIC_PROP = "public"

  implicit override val modelFormat: Format[Dictionary] = Json.format[Dictionary]

}
