package vocabulary.domain.model

import common.domain.model.{BaseModelFormatter, BaseModel}
import play.api.libs.json.Format


case class ItemContent( var value: Option[String] = None,
                        var cType: Option[String] = None) extends BaseModel

object ItemContent extends BaseModelFormatter[ItemContent] {
  val VALUE_PROP = "value"
  val CONTENT_TYPE_PROP = "cType"

  import play.api.libs.json.Json

  override implicit val modelFormat: Format[ItemContent] = Json.format[ItemContent]

}


