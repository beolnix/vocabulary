package vocabulary.domain.model

import common.domain.model.BaseModel

case class CardPreSet(id: Option[String],
                      userId: Option[String],
                      name: String,
                      cardTypeIds: Map[Number, String]) extends BaseModel {

}
