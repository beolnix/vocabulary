package vocabulary.domain.model

import play.api.libs.json.{Json, Format, JsObject, JsValue}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import common.domain.model.{BaseModel, BaseModelFormatter}

case class BookContent( var id: Option[String],
                        var content: String,
                        var bookId: String ) extends BaseModel

object BookContent extends BaseModelFormatter[BookContent]{
  val CONTENT_PROP = "content"
  val BOOK_ID_PROP = "bookId"

  implicit override val modelFormat: Format[BookContent] = Json.format[BookContent]

}


