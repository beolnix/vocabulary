package vocabulary.domain.model

import common.domain.model.{BaseModelFormatter, BaseModel}
import play.api.libs.json.Format

case class RepeatPeriod( var orderNum: Int = 0,
                         var periodVal: Int ) extends BaseModel

object RepeatPeriod extends BaseModelFormatter[RepeatPeriod] {
  val ORDER_NUM_PROP = "orderNum"
  val PERIOD_VAL_PROP = "periodVal"

  import play.api.libs.json.Json

  override implicit val modelFormat: Format[RepeatPeriod] = Json.format[RepeatPeriod]

}
