package vocabulary.domain.model

import play.api.libs.json.{Json, Format, JsObject, JsValue}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import common.domain.model.{BaseModelFormatter, BaseModel}

case class Book( var id: Option[String],
                 var userId: Option[String],
                 var offset: Long,
                 var length: Long,
                 var items: Array[String],
                 var name: String,
                 var lang: String,
                 var pageSize: Long = 4000) extends BaseModel

object Book extends BaseModelFormatter[Book] {
  val OFFSET_PROP = "offset"
  val LENGTH_PROP = "length"
  val ITEMS_PROP = "items"
  val LANG_PROP = "lang"
  val PAGE_SIZE_PROP = "pageSize"

  implicit override val modelFormat: Format[Book] = Json.format[Book]
  
}
