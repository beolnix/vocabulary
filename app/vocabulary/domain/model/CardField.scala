package vocabulary.domain.model

import common.domain.model.{BaseModelFormatter, BaseModel}
import play.api.libs.json.{Json, Format}

case class CardField(name: String,
                     ctype: String,
                     widget: String,
                     refKeyName: Option[String],
                     refName: Option[String]) extends BaseModel

object CardField extends BaseModelFormatter[CardField] {

  implicit override val modelFormat: Format[CardField] = Json.format[CardField]

  val CTYPE_PROP = "ctype"
  val WIDGET_PROP = "widget"
  val REF_KEY_NAME_PROP = "refKeyName"
  val REF_NAME_PROP = "refName"

}
