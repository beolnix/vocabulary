package vocabulary.domain.model

class MissedCategory extends ValidationError("catId", "Category hasn't been provided", ValidationErrorType.NULL_VALUE) {

}
