package vocabulary.domain.model

import common.domain.model.{BaseModelFormatter, BaseModel}
import play.api.libs.json.{Json, Format}

case class CardType(var id: Option[String],
                    var name: String,
                    var userId: Option[String],
                    var frontFields: Seq[CardField],
                    var backFields: Seq[CardField]) extends BaseModel

object CardType extends BaseModelFormatter[CardType] {

  implicit override val modelFormat: Format[CardType] = Json.format[CardType]

  val FRONT_FIELDS_PROP = "frontFields"
  val BACK_FIELDS_PROP = "backFields"

}


