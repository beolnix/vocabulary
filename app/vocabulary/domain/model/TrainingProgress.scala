package vocabulary.domain.model

import java.util.Date
import common.domain.model.{BaseModelFormatter, BaseModel}
import play.api.libs.json.Format

case class TrainingProgress( var periodNum: Int = 0,
                             var repeatDt: Option[Date] = None) extends BaseModel

object TrainingProgress extends BaseModelFormatter[TrainingProgress] {
  val PERIOD_NUM_PROP = "periodNum"
  val REPEAT_DATE = "repeatDt"

  import play.api.libs.json.Json

  override implicit val modelFormat: Format[TrainingProgress] = Json.format[TrainingProgress]

}
