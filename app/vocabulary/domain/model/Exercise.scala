package vocabulary.domain.model

import common.domain.model.{BaseModelFormatter, BaseModel}
import play.api.libs.json.Format

case class Exercise( var orderNum: Int = 0,
                     var exType: String = ExersiceType.ShowFrontHideBack ) extends BaseModel

object Exercise extends BaseModelFormatter[Exercise] {

  val ORDER_NUM_PROP = "orderNum"
  val EX_TYPE_PROP = "exType"

  import play.api.libs.json.Json

  override implicit val modelFormat: Format[Exercise] = Json.format[Exercise]
}
