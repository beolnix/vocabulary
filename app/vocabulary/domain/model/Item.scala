package vocabulary.domain.model

import play.api.libs.json.{Format, Json, JsObject, JsValue}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import common.domain.model.{BaseModelFormatter, BaseModel}

case class Item( var id: Option[String] = None,
                 var userId: Option[String] = None,
                 var catId: Option[String],
                 var cardTypeId: String,
                 var progress: TrainingProgress,
                 var values: Map[String, ItemContent]) extends BaseModel

object Item extends BaseModelFormatter[Item] {

  val CAT_ID_PROP = "catId"
  val VALUES_PROP = "values"
  val TRAINING_PROGRESS_PROP = "progress"
  val CARD_TYPE_ID_PROP = "cardTypeId"

  implicit override val modelFormat: Format[Item] = Json.format[Item]

}