package vocabulary.domain.model

import play.api.libs.json.{Json, Format, JsObject, JsValue}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import common.domain.model.{BaseModel, BaseModelFormatter}

case class Category(var id: Option[String],
                    var userId: Option[String],
                    var name: String,
                    var currentExercises: Array[Exercise],    //Упражнения для изучения новых слов
                    var periods: Array[RepeatPeriod],
                    var repeatMax: Int,                       //Количество изученных случайных слов в каждой тренировке для повторения
                    var currentMax: Int,                      //Количество слов в каждой тренировке для изучения
                    var repeatExercises: Array[Exercise]) extends BaseModel

object Category extends BaseModelFormatter[Category] {
  val CURRENT_EXERCISES_PROP = "currentExercises"
  val PERIODS_PROP = "periods"
  val REPEAT_MAX_PROP = "repeatMax"
  val CURRENT_MAX_PROP = "currentMax"
  val REPEAT_EXERCISES_PROP = "repeatExercises"
  val COUNTERS_PROP = "counters"

  implicit override val modelFormat: Format[Category] = Json.format[Category]
}
