package vocabulary.domain.dao

import vocabulary.domain.model._
import vocabulary.domain.model.Item._


import scala.concurrent._
import ExecutionContext.Implicits.global


import reactivemongo.bson._

import reactivemongo.core.commands.Count
import play.api.libs.json.{JsString, Json, JsObject}
import play.modules.reactivemongo.json.collection.JSONCollection
import common.domain.dao.BaseJsonDAO
import common.domain.model.{BaseModel}
import reactivemongo.api.indexes.{IndexType, Index}

object ItemJsonDAO extends BaseJsonDAO {
  val collection = db.collection[JSONCollection]("item")
//  collection.indexesManager.ensure(
//    new Index(
//      Seq(
//        (Item.FRONT_PROP + "." + ItemContent.VALUE_PROP, IndexType.Text),
//        (Item.BACK_PROP + "." + ItemContent.VALUE_PROP, IndexType.Text)
//        )
//      )
//    )

  def findByCategoryIdAndSearchKey(userId: String, categoryId: String, searchKey: String) = {
    var selector = Json.obj(BaseModel.USER_ID_PROP -> userId)

    if (categoryId != "") {
      selector = selector + (CAT_ID_PROP -> JsString(categoryId))
    }

    if (searchKey != "") {
      selector = selector + ( "$text" -> Json.obj( "$search" -> JsString(searchKey) ) )
    }

    collection.find(selector).cursor[JsObject].collect[List]()
  }

  def findByCategoryId(categoryId: String): Future[List[JsObject]] = {
    val selector = Json.obj(Item.CAT_ID_PROP -> categoryId)
    collection.find(selector).cursor[JsObject].collect[List]()
  }

//  def findByFrontAndUserId(front: JsObject, userId: String): Future[Option[JsObject]] = {
//    val selector = Json.obj(BaseModel.USER_ID_PROP -> userId,
//      Item.FRONT_PROP -> front)
//    collection.find(selector).cursor[JsObject].headOption
//  }

  def findRepeatWords(userId: String, categoryId: String, limit: Int, exeptIds: Array[String]): Future[List[JsObject]] = {
    val selector = Json.obj(BaseModel.USER_ID_PROP -> userId,
      Item.CAT_ID_PROP -> categoryId,
      Item.TRAINING_PROGRESS_PROP + "." + TrainingProgress.REPEAT_DATE ->
        Json.obj("$gt" -> System.currentTimeMillis()),
      "_id" ->
        Json.obj("$nin" -> Json.arr(exeptIds))
    )
    collection.find(selector).sort(Json.obj(Item.TRAINING_PROGRESS_PROP + "." + TrainingProgress.REPEAT_DATE -> 1))
      .cursor[JsObject].collect[List](limit)
  }

  def findAllItemsForIds(itemIds: Array[String]): Future[List[JsObject]] = {
    val selector = Json.obj(BaseModel.ID_PROP ->
      Json.obj("$in" -> Json.arr(itemIds.toTraversable))
    )
    collection.find(selector).cursor[JsObject].collect[List]()
  }

  def findCurrentPeriodWords(userId: String, categoryId: String, limit: Int, exeptIds: Array[String]): Future[List[JsObject]] = {
    val selector = Json.obj(BaseModel.USER_ID_PROP -> userId,
      Item.CAT_ID_PROP -> categoryId,
      Item.TRAINING_PROGRESS_PROP + "." + TrainingProgress.REPEAT_DATE ->
        Json.obj("$lt" -> System.currentTimeMillis()),
      BaseModel.ID_PROP ->
        Json.obj("$nin" -> Json.arr(exeptIds))
    )
    collection.find(selector).cursor[JsObject].collect[List](limit)
  }

  def findNewWords(userId: String, categoryId: String, limit: Int, exeptIds: Array[String]): Future[List[JsObject]] = {
    val selector = Json.obj(BaseModel.ID_PROP -> userId,
      Item.CAT_ID_PROP -> categoryId,
      BaseModel.ID_PROP ->
        Json.obj("$nin" -> Json.arr(exeptIds)))
    collection.find(selector).cursor[JsObject].collect[List]()
  }

  def countAllForCategory(categoryId: String): Future[Int] = {
    val countCommand = new Count(collection.name, Option(BSONDocument(Item.CAT_ID_PROP -> BSONObjectID(categoryId))))

    db.command(countCommand)
  }

//  def countAllNewWords(categoryId: String): Future[Int] = {
//    val selector = BSONDocument(Item.TRAINING_PROGRESS_PROP + "." + TrainingProgress.STAGE_PROP ->
//      BSONString(TrainingStage.New.toString),
//      Item.CATID_PROP -> BSONObjectID(categoryId))
//    val countCommand = Count(collection.name, Option(selector))
//    db.command(countCommand)
//  }

//  def countAllPractiseWords(categoryId: String): Future[Int] = {
//    val selector = BSONDocument(Item.TRAINING_PROGRESS_PROP + "." + TrainingProgress.STAGE_PROP ->
//      BSONString(TrainingStage.Practise.toString),
//      Item.CATID_PROP -> BSONObjectID(categoryId),
//      Item.TRAINING_PROGRESS_PROP + "." + TrainingProgress.PERIOD_START_DATE_PROP ->
//        BSONDocument("$lt" -> BSONLong(System.currentTimeMillis()))
//    )
//
//    val countCommand = Count(collection.name, Option(selector))
//    db.command(countCommand)
//  }
//
//  def countLearnedWords(categoryId: String): Future[Int] = {
//    val selector = BSONDocument(Item.TRAINING_PROGRESS_PROP + "." + TrainingProgress.STAGE_PROP ->
//      BSONString(TrainingStage.Practise.toString),
//      Item.CATID_PROP -> BSONObjectID(categoryId),
//      Item.TRAINING_PROGRESS_PROP + "." + TrainingProgress.PERIOD_START_DATE_PROP ->
//        BSONDocument("$gt" -> BSONLong(System.currentTimeMillis())))
//    BSONDocument("$gt" -> BSONLong(System.currentTimeMillis()))
//    val countCommand = Count(collection.name, Option(selector))
//    db.command(countCommand)
//  }

}
