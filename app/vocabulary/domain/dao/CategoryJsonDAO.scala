package vocabulary.domain.dao

import scala.concurrent.{Future, ExecutionContext}
import ExecutionContext.Implicits.global
import play.modules.reactivemongo.json.collection.JSONCollection
import vocabulary.domain.model.{Category, Item}
import common.domain.dao.BaseJsonDAO
import play.api.libs.json.{Json, JsObject}
import common.domain.model.{BaseModel}


object CategoryJsonDAO extends BaseJsonDAO {
  val collection = db.collection[JSONCollection]("category")

  // removes links to this category from items
  private def deleteLinks(categoryId: String) {
    val itemsFuture = ItemJsonDAO.findByCategoryId(categoryId)
    itemsFuture.map {items =>
      for (item <- items) {
        val itemWithoutCategory = item - Item.CAT_ID_PROP
        ItemJsonDAO.save(itemWithoutCategory)
      }
    }
  }

}
