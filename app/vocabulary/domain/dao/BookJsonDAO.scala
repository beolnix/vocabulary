package vocabulary.domain.dao

import play.modules.reactivemongo.json.collection.JSONCollection
import common.domain.dao.BaseJsonDAO

object BookJsonDAO extends BaseJsonDAO {
  val collection = db.collection[JSONCollection]("book")
}
