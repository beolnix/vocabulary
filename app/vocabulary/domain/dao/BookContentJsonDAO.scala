package vocabulary.domain.dao

import vocabulary.domain.model.BookContent._
import scala.concurrent.ExecutionContext
import scala.concurrent.Future

import ExecutionContext.Implicits.global
import play.api.libs.json.{JsObject, Json}
import vocabulary.domain.model.BookContent
import play.modules.reactivemongo.json.collection.JSONCollection
import common.domain.dao.BaseJsonDAO

object BookContentJsonDAO extends BaseJsonDAO {
  val collection = db.collection[JSONCollection]("bookContent")

  def findByBookId(bookId: String): Future[Option[JsObject]] = {
    val selector = Json.obj(BookContent.BOOK_ID_PROP -> bookId)
    val cursor = collection.find(selector).cursor[JsObject]
    cursor.headOption
  }

}
