package vocabulary.domain.dao

import vocabulary.domain.model.{ContentType, ItemContent, DictionaryItem}
import vocabulary.domain.model.DictionaryItem._

import scala.concurrent._
import ExecutionContext.Implicits.global

import play.api.libs.json.{JsObject, Json}
import play.modules.reactivemongo.json.collection.JSONCollection
import common.domain.dao.BaseJsonDAO

object DictionaryItemDAO extends BaseJsonDAO {
  val collection = db.collection[JSONCollection]("dictionaryItem")

  def findWordInDictionary(dictionaryId: String, word: String): Future[Option[JsObject]] = {
    val selector = Json.obj(DictionaryItem.KEY_PROP ->
                              Json.obj(DictionaryItem.VALUE_PROP -> word.toLowerCase,
                                     ItemContent.CONTENT_TYPE_PROP -> ContentType.TEXT_TYPE),
                        DictionaryItem.DICTIONARY_ID_PROP -> dictionaryId
                      )
    collection.find(selector).cursor[JsObject].headOption
  }

  def findAllByDictionaryId(dictionaryId: String): Future[List[JsObject]] = {
    val selector = Json.obj(DictionaryItem.DICTIONARY_ID_PROP -> dictionaryId)
    collection.find(selector).cursor[JsObject].collect[List]()
  }

}
