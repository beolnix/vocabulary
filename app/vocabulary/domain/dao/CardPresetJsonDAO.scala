package vocabulary.domain.dao

import common.domain.dao.BaseJsonDAO
import play.modules.reactivemongo.json.collection.JSONCollection

import scala.concurrent._
import ExecutionContext.Implicits.global

import play.api.libs.json.{JsString, Json, JsObject}
import common.domain.model.BaseModel
import vocabulary.domain.model.{CardPreset, CardType}
import vocabulary.service.CardPresetService

object CardPresetJsonDAO extends BaseJsonDAO {
  val collection = db.collection[JSONCollection]("cardpreset")

  val DEFAULT_CARD_PRESET_NAME = "default"
  val DEFAULT_CARD_PRESET_DESC = "text front / text back"

  def getAllPresetsWithDefault(userId: String): Future[Seq[JsObject]] = {
    findByUserId(userId).flatMap { resultList =>
      var containsDefault = false

      for (preset <- resultList) {
        if (preset.asInstanceOf[CardPreset].name == DEFAULT_CARD_PRESET_NAME)
          containsDefault = true
      }

      if (containsDefault)
        Future.successful(resultList)
      else {
        createDefaultPreset().map { defaultPreset =>
          resultList.:+(defaultPreset)
        }
      }
    }
  }

  private def createDefaultPreset(): Future[JsObject] = {

    CardTypeJsonDAO.getDefaultFrontBackCardType().flatMap { frontBack =>
      CardTypeJsonDAO.getDefaultBackFrontCardType().flatMap { backFront =>
        val defaultMap = Array[String](frontBack.asInstanceOf[CardType].id.get,
                                       backFront.asInstanceOf[CardType].id.get)
        val defaultCardPreset = CardPreset(None, None, DEFAULT_CARD_PRESET_NAME,
          DEFAULT_CARD_PRESET_DESC, defaultMap)
        CardPresetJsonDAO.save(defaultCardPreset).map { savedDefaultCardPreset =>
          savedDefaultCardPreset
        }
      }
    }
  }

}
