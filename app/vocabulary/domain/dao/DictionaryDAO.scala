package vocabulary.domain.dao

import vocabulary.domain.model.Dictionary
import vocabulary.domain.model.Dictionary._

import scala.concurrent._
import ExecutionContext.Implicits.global
import reactivemongo.bson.BSONBoolean

import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.libs.json.{JsObject, Json}
import common.domain.dao.BaseJsonDAO
import common.domain.model.{BaseModel}

object DictionaryDAO extends BaseJsonDAO {
  val collection = db.collection[JSONCollection]("dictionary")

  def findAllVisible(userId: String): Future[List[JsObject]] = {
    val selectCondition = Json.arr(Json.obj(BaseModel.USER_ID_PROP -> userId),
                                   Json.obj(Dictionary.PUBLIC_PROP -> true ))

    val selector = Json.obj("$or" -> selectCondition)
    collection.find(selector).cursor[JsObject].collect[List]()
  }

  def findDictionariesForField(fieldName: String, userId: String): Future[List[JsObject]] = {
    val or = Json.arr( Json.obj(BaseModel.USER_ID_PROP -> userId),
                       Json.obj(Dictionary.PUBLIC_PROP -> true) )
    val and = Json.arr(Json.obj(Dictionary.FIELD_NAME_PROP -> fieldName),
                        Json.obj("$or" -> or))
    val selector = Json.obj("$and" -> and)
    collection.find(selector).cursor[JsObject].collect[List]()
  }
}
