package vocabulary.domain.dao

import common.domain.dao.BaseJsonDAO
import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.libs.json.{Json, JsObject}
import common.domain.model.BaseModel
import vocabulary.domain.model.{CardField, CardType}

import scala.concurrent._
import ExecutionContext.Implicits.global

object CardTypeJsonDAO extends BaseJsonDAO {
  val collection = db.collection[JSONCollection]("cardtype")

  val DEFAULT_FRONT_BACK_CARD_TYPE_NAME = "Default front/back"
  val DEFAULT_BACK_FRONT_CARD_TYPE_NAME = "Default back/front"

  def getAllCardTypesWithDefault(userId: String): Future[Seq[JsObject]] = {
    findByUserId(userId).flatMap{ resultList =>

      var containsFrontBack = false
      var containsBackFront = false

      for (cardType <- resultList) {
        if (cardType.asInstanceOf[CardType].name == DEFAULT_FRONT_BACK_CARD_TYPE_NAME)
          containsFrontBack = true
        if (cardType.asInstanceOf[CardType].name == DEFAULT_BACK_FRONT_CARD_TYPE_NAME)
          containsBackFront = true
      }

      if (containsFrontBack && containsBackFront)
        Future.successful(resultList)
      else {
        getDefaultBackFrontCardType.flatMap { backFront =>
          getDefaultFrontBackCardType.map { frontBack =>
            resultList.+:(backFront).+:(frontBack)
          }
        }
      }
    }
  }

  def getDefaultFrontBackCardType(): Future[JsObject] = {
    val futureFrontBackCardType = CardTypeJsonDAO.findByName(DEFAULT_FRONT_BACK_CARD_TYPE_NAME)
    futureFrontBackCardType.flatMap { frontBackCardType =>
      if (frontBackCardType.isDefined)
        Future.successful(frontBackCardType.get)
      else {
        createDefaultFrontBackCardType()
      }
    }
  }

  def getDefaultBackFrontCardType(): Future[JsObject] = {
    val futureBackFrontCardType = CardTypeJsonDAO.findByName(DEFAULT_BACK_FRONT_CARD_TYPE_NAME)
    futureBackFrontCardType.flatMap { backFront =>
      if (backFront.isDefined)
        Future.successful(backFront.get)
      else {
        createDefaultBackFrontCardType()
      }
    }
  }

  private def createDefaultFrontBackCardType(): Future[JsObject] = {

    CardTypeJsonDAO.findByName(DEFAULT_FRONT_BACK_CARD_TYPE_NAME).flatMap { frontBack =>
      if (frontBack.isDefined)
        Future.successful(frontBack.get)
      else {
        val frontField = CardField("front", "text", "label", None, None)
        val backField = CardField("back", "text", "label", None, None)

        val frontBack = CardType(None, DEFAULT_FRONT_BACK_CARD_TYPE_NAME,
          None, Seq[CardField](frontField), Seq[CardField](backField))

        CardTypeJsonDAO.save(frontBack).map { savedFrontBack =>
          savedFrontBack
        }
      }

    }
  }

  private def createDefaultBackFrontCardType(): Future[JsObject] = {

    CardTypeJsonDAO.findByName(DEFAULT_BACK_FRONT_CARD_TYPE_NAME).flatMap { backFront =>
      if (backFront.isDefined)
        Future.successful(backFront.get)
      else {
        val frontField = CardField("front", "text", "label", None, None)
        val backField = CardField("back", "text", "label", None, None)

        val frontBack = CardType(None, DEFAULT_FRONT_BACK_CARD_TYPE_NAME,
          None, Seq[CardField](backField), Seq[CardField](frontField))

        CardTypeJsonDAO.save(frontBack).map { savedBackFront =>
          savedBackFront
        }
      }

    }
  }

}
