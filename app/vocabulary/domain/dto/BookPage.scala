package vocabulary.domain.dto

import vocabulary.domain.model.Item
import play.api.libs.json._
import common.domain.model.{BaseModel, BaseModelFormatter}

case class BookPage( items: Array[Item],
                     content: String,
                     nextPageOffset: Long,
                     prevPageOffset: Long ) extends BaseModel

object BookPage extends BaseModelFormatter[BookPage] {
  val ITEMS_PROP = "items"
  val CONTENT_PROP = "content"
  val NEXT_PROP = "nextPageOffset"
  val PREV_PROP = "prevPageOffset"

  import play.api.libs.json.Json

  override implicit val modelFormat: Format[BookPage] = Json.format[BookPage]
}
