package vocabulary.domain.dto

import play.api.libs.json.{Json, Format, JsObject, JsValue}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import common.domain.model.{BaseModelFormatter, BaseModel}

case class StatisticsDTO(all: Int = 0,
                         onRepeat: Int = 0,
                         onLearn: Int = 0,
                         newlyAdded: Int = 0,
                         catId: String ) extends BaseModel

object StatisticsDTO extends BaseModelFormatter[StatisticsDTO] {

  val ALL_PROP = "all"
  val ON_REPEAT_PROP = "onRepeat"
  val ON_LEARN_PROP = "onLearn"
  val CAT_ID_PROP = "catId"
  val NEWLY_ADDED_PROP = "newlyAdded"

  implicit override val modelFormat: Format[StatisticsDTO] = Json.format[StatisticsDTO]

}