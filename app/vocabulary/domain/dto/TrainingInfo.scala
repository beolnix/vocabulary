package vocabulary.training.dto

import vocabulary.domain.model.{Category, Item}
import play.api.libs.json._
import common.domain.model.{BaseModel, BaseModelFormatter}

case class TrainingInfo( var category: Category,
                         var items: List[Item] ) extends BaseModel

object TrainingInfo extends BaseModelFormatter[TrainingInfo] {

  val CATEGORY_PROP = "category"
  val ITEMS_PROP = "items"

  import play.api.libs.json.Json

  override implicit val modelFormat: Format[TrainingInfo] = Json.format[TrainingInfo]
}
