package vocabulary.rest

import play.api.mvc._
import play.api.libs.json._

import vocabulary.domain.model.{ValidationError, Item}

import vocabulary.domain.dao.ItemJsonDAO

import scala.concurrent._
import ExecutionContext.Implicits.global

import security.SecurityWebUtils
import common.domain.model.{BaseModel}
import vocabulary.service.StatisticsService
import vocabulary.validation.{ValidationWebUtils, ItemValidationService}

object ItemRESTFacade extends Controller with SecurityWebUtils with ValidationWebUtils {

  val saveValidators = Seq[Item => Option[ValidationError]](ItemValidationService.checkCategory _)

  def get(itemId: String) = Action.async {
    implicit request =>
      authenticated {
        (user) =>
          ItemJsonDAO.findByIdAndUserId(user.id.get, itemId).map {
            item =>
              if (item.isDefined)
                Ok(item.get)
              else
                NotFound
          }
      }
  }

  def save(itemId: String = "") = Action.async(parse.json) {
    implicit request =>
      authenticated {
        (user) =>
          val json = request.body.as[JsObject]
          val item = json +(BaseModel.USER_ID_PROP, JsString(user.id.get))

          validate(item, saveValidators: _*) {
            () =>
              ItemJsonDAO.save(item).map {
                result =>
                  StatisticsService.clearStatisticsCache(session.get("uuid").get, user.id.get)
                  Ok(result)
              }
          }
      }
  }

  def create = Action.async(parse.json) {
    implicit request =>
      authenticated {
        (user) =>
          val json = request.body.as[JsObject]
          val item = json +(BaseModel.USER_ID_PROP, JsString(user.id.get))
          validate(item, saveValidators: _*) {
            () =>
//              val existCheckFuture = ItemJsonDAO.findByFrontAndUserId((item \ Item.FRONT_PROP).as[JsObject], user.id.get)
//              existCheckFuture.flatMap {
//                existCheck =>
//                  if (!existCheck.isDefined) {
                    ItemJsonDAO.save(item).map {
                      result =>
                        StatisticsService.clearStatisticsCache(session.get("uuid").get, user.id.get)
                        Ok(result)
//                    }
//                  } else {
//                    Future.successful(Conflict(Json.toJson(existCheck.get)))
//                  }
              }
          }

      }
  }

  def search(categoryId: String = "", searchKey: String = "") = Action.async {
    implicit request =>
      authenticated {
        (user) =>
          var result: Future[List[JsObject]] = null
          if (categoryId == "" && searchKey == "") {
            result = ItemJsonDAO.findByUserId(user.id.get)
          } else {
            result = ItemJsonDAO.findByCategoryIdAndSearchKey(user.id.get, categoryId, searchKey)
          }

          result.map {
            items =>
              Ok(Json.toJson(items))
          }
      }
  }

}
