package vocabulary.rest

import play.api.mvc._
import play.api.libs.json._

import security.SecurityWebUtils
import vocabulary.domain.dao.BookJsonDAO
import vocabulary.domain.model.Book
import vocabulary.domain.model.Book._

import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global

object BookRESTFacade extends Controller with SecurityWebUtils {

  def get(bookId: String) = Action.async {
    implicit request =>
      authenticated {
        user =>
          val result = BookJsonDAO.findById(bookId)
          result.map {
            book =>
              if (book.isDefined)
                Ok(book.get)
              else
                NotFound
          }
      }
  }

  def save(bookId: String) = Action.async(parse.json) {
    implicit request =>
      authenticated {
        user =>
          val book = request.body.as[JsObject].as[Book]
          book.userId = user.id

          BookJsonDAO.save(book).map {
            result => Ok(result)
          }
      }
  }
}