package vocabulary.rest

import play.api.mvc._
import play.api.libs.json._

import security.SecurityWebUtils
import vocabulary.domain.dao.CategoryJsonDAO
import vocabulary.domain.model.Category._
import vocabulary.domain.model.Category

import scala.concurrent.{Future, ExecutionContext}
import ExecutionContext.Implicits.global
import common.domain.model.{BaseModel}
import vocabulary.service.CategoryService

object CategoryRESTFacade extends Controller with SecurityWebUtils {

  def get( categoryId: String ) = Action.async { implicit request =>
    authenticated { (user) =>
        CategoryJsonDAO.findByIdAndUserId(user.id.get, categoryId).map { category =>
          if (category.isDefined)
            Ok(category.get)
          else
            NotFound
        }
    }
  }

  def list = Action.async { implicit request =>
    authenticated { (user) =>
        CategoryJsonDAO.findByUserId(user.id.get).flatMap { list =>
          if (list.size == 0) {
            CategoryService.getDefaultCategory(user.id.get).map( defaultCategory =>
              Ok(Json.toJson(Array(defaultCategory)))
            )
          } else {
            Future.successful(Ok(Json.toJson(list)))
          }
        }
      }
  }

  def save( categoryId: String ) = Action.async(parse.json) { implicit request =>
    authenticated { user =>
      val json = request.body.as[JsObject]
      val category = json + (BaseModel.USER_ID_PROP, JsString(user.id.get))
      CategoryJsonDAO.save(category).map { result => Ok(result) }
    }
  }

  def create = Action.async(parse.json) { implicit request =>
    authenticated { (user) =>
      val json = request.body.as[JsObject]
      val category = json + (BaseModel.USER_ID_PROP, JsString(user.id.get))
      CategoryJsonDAO.save(category).map { result => Ok(result) }
    }
  }

  def delete(categoryId: String) = Action.async { implicit request =>
    authenticated { (user) =>
      CategoryJsonDAO.delete(categoryId).map { result => Ok }
    }
  }
}
