package vocabulary.validation

import vocabulary.domain.model.{MissedCategory, ValidationError, Item}

object ItemValidationService {

  def checkCategory(item: Item): Option[ValidationError] = {
    if (item.catId.isEmpty)
      return Option(new MissedCategory)

    None
  }

}
