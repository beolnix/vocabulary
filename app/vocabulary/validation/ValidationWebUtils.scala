package vocabulary.validation

import common.domain.model.BaseModel
import play.api.mvc.{SimpleResult}
import play.api.mvc.Results._

import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import scala.collection.mutable
import play.api.libs.json._
import vocabulary.domain.model.ValidationError

trait ValidationWebUtils {

  def validate[M <: BaseModel](model: M, validateFunctions: ((M) => Option[ValidationError])*)
                              (actionFunction: (  ) => Future[SimpleResult]): Future[SimpleResult] = {

    val errors = mutable.ArrayBuffer[ValidationError]()

    for (validateFunction <- validateFunctions) {
      val validationResult: Option[ValidationError] = validateFunction(model)
      if (validationResult.isDefined) {
        errors += validationResult.get
      }
    }

    if (errors.size > 0) {
      Future.successful(BadRequest(Json.toJson(errors.toArray)))
    } else {
      actionFunction()
    }

  }

  def validate[ M <: BaseModel](jsObj: JsObject, validateFunctions: ((M) => Option[ValidationError])*)
                            (actionFunction: (  ) => Future[SimpleResult])
                            (implicit convertion: ((JsObject) => M)): Future[SimpleResult] = {

    val model = convertion(jsObj)
    validate(model, validateFunctions: _*)(actionFunction)

  }

}
