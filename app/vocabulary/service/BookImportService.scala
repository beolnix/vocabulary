package vocabulary.service

import vocabulary.domain.model.{BookContent, Book}
import java.io.File
import vocabulary.domain.dao.{BookJsonDAO, BookContentJsonDAO}
import play.api.libs.json.{JsObject, Json}


object BookImportService {

  def importBook(file: File, book: Book) {
    val source = scala.io.Source.fromFile(file)
    val content = source.getLines mkString "\n"

    val bookContent = new BookContent(None, content, book.id.get)
    BookContentJsonDAO.save(bookContent)

    book.length = content.length
    BookJsonDAO.save(book)
  }
}
