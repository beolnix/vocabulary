package vocabulary.service

import scala.collection.mutable
import vocabulary.domain.dto.StatisticsDTO
import play.api.cache.Cache
import vocabulary.domain.dao.ItemJsonDAO
import java.util.Date
import vocabulary.domain.model.Item._

import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global

import play.api.Play.current
import common.service.MyAppConfig
import vocabulary.domain.model.{Category}

object StatisticsService {

  val cacheKeySuffix: String = "-statistics"

  def clearStatisticsCache(uuid: String, userId: String) {
    val cacheKey = uuid + cacheKeySuffix
    val cachedMap = Cache.get(cacheKey).asInstanceOf[Option[mutable.HashMap[String, StatisticsDTO]]]
    if (cachedMap.isDefined) {
      Cache.remove(cacheKey)
    }
  }

  def getStatisticsMap(uuid: String, userId: String): Future[mutable.HashMap[String, StatisticsDTO]] = {
    val cacheKey = uuid + cacheKeySuffix
    val cachedMap = Cache.get(cacheKey).asInstanceOf[Option[mutable.HashMap[String, StatisticsDTO]]]
    if (cachedMap.isEmpty) {
      val statisticsMap: mutable.HashMap[String, StatisticsDTO] = new mutable.HashMap[String, StatisticsDTO]()
      CategoryService.getDefaultCategory(userId).flatMap { defaultCategory =>
        countItems(userId, defaultCategory, statisticsMap, cacheKey).map { result => result }
      }
    } else {
      Future.successful(cachedMap.get)
    }

  }

  def countItems(userId: String, defaultCategory: Category, statisticsMap: mutable.HashMap[String, StatisticsDTO], cacheKey: String): Future[mutable.HashMap[String, StatisticsDTO]] = {
    val result = ItemJsonDAO.findByUserId(userId)
    result.map { items =>
      items map { item =>

        var catId = item.catId
        if (catId.isEmpty) {
          catId = defaultCategory.id
        }

        var statistics = statisticsMap.get(catId.get)
        if (statistics.isEmpty) {
          statistics = Option(new StatisticsDTO(0, 0, 0, 0,catId.get))
        }

        val allCounter = statistics.get.all + 1
        var newCounter = statistics.get.newlyAdded
        var onRepeatCounter = statistics.get.onRepeat
        var onLearnCounter = statistics.get.onLearn

        if (item.progress.repeatDt.isEmpty) {
          newCounter += 1
        } else if (item.progress.repeatDt.get.before(new Date())) {
          onLearnCounter += 1
        } else if (item.progress.repeatDt.get.after(new Date())) {
          onRepeatCounter += 1
        }

        val newStatistics = new StatisticsDTO(allCounter, onRepeatCounter, onLearnCounter, newCounter, catId.get)
        statisticsMap.put(catId.get, newStatistics)
      }

      Cache.set(cacheKey, statisticsMap, MyAppConfig.cacheDuration)
      statisticsMap
    }

  }
}
