package vocabulary.service

import vocabulary.domain.dao.DictionaryItemDAO
import java.io.File
import vocabulary.domain.model._

object SourceImportService {
  def importDictionary(file: File, dictionary: Dictionary) {
    val fileLines = io.Source.fromFile(file).getLines.toList
    for (line <- fileLines) {
      val result: Array[String] = line.split(" -- ").map(arg => arg.trim())

      val dicItem = new DictionaryItem(None,
        dictionary.id.get, new ItemContent(Option(result(0)), Option(ContentType.TEXT_TYPE)),
        new ItemContent(Option(result(1)), Option(ContentType.TEXT_TYPE)))

      DictionaryItemDAO.save(dicItem)
    }
  }
}
