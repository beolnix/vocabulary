package vocabulary.service

import vocabulary.domain.model.{ExersiceType, Exercise, Category, RepeatPeriod}
import vocabulary.domain.dao.CategoryJsonDAO
import scala.concurrent.{Future, ExecutionContext}
import ExecutionContext.Implicits.global

object CategoryService {
  val DEFAULT_CAT_NAME = "default"

  def getDefaultCategory(userId: String): Future[Category] = {
    CategoryJsonDAO.findByNameAndSpecificUserId(userId, DEFAULT_CAT_NAME) flatMap { defaultCat =>
      if (defaultCat.isDefined) {
        Future.successful(Category.readObject(defaultCat.get))
      } else {
        val defaultCat = createCategoryForName(userId, DEFAULT_CAT_NAME)
        CategoryJsonDAO.save(defaultCat).map(category => category)
      }
    }
  }

  def createCategoryForName(userId: String, catName: String): Category = {
    new Category(None, Option(userId), catName,
      Array(new Exercise(1, ExersiceType.ShowFrontHideBack),
            new Exercise(2, ExersiceType.ShowBackTypeFront)),
      Array(new RepeatPeriod(1, 8), new RepeatPeriod(2, 16)),
      5, 5,
      Array(new Exercise(1, ExersiceType.ShowFrontHideBack),
        new Exercise(2, ExersiceType.ShowBackTypeFront))
    )
  }
}
