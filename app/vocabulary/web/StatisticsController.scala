package vocabulary.web

import play.api.mvc.{Controller, Action}
import security.SecurityWebUtils
import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global
import play.api.libs.json.Json
import vocabulary.service.StatisticsService


object StatisticsController extends Controller with SecurityWebUtils {

  def statisticsList = Action.async {
    implicit request =>
      authenticated {
        user =>
          StatisticsService.getStatisticsMap(session.get("uuid").get, user.id.get) map { statisticsMap =>
            Ok(Json.toJson(statisticsMap.toMap))
          }
      }
  }

}
