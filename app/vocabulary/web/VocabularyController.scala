package vocabulary.web

import play.api.mvc._
import security.SecurityWebUtils


import scala.concurrent.{Future, ExecutionContext}
import ExecutionContext.Implicits.global

import reactivemongo.api.Cursor
import assets.js.test.layout.BrowserDetection

object VocabularyController extends Controller with SecurityWebUtils {

  def show = Action.async {
    implicit request =>
      authenticated {
        user =>
          Future(Ok(views.html.full.vocabulary(true)))
      }
  }

}
