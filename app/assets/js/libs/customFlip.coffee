define [
  'jquery'

], ($) ->
  $.fx.step.customFlip = (fx) ->
    if (fx.now >= 90)
      $(fx.elem).find('#front').css('display', 'none')
      $(fx.elem).find('#back').css('display', 'block')

    if (fx.now >= 90)
      $(fx.elem).css('transform', 'rotateY(' + (180 - fx.now) + 'deg)')
    else
      $(fx.elem).css('transform', 'rotateY(' + fx.now + 'deg)')

  $