define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  'tools/Mediator'

  #template
  'text!/assets/js/full/RibbonTemplate.html'

], (Backbone, $, _, Mediator, ribbonTemplate) ->
  RibbonView = Backbone.View.extend
    tagName: "div"
    caption: null

    init: (caption) ->
      this.caption = caption

    render: () ->
      t = _.template(ribbonTemplate.replace('%NAME%', @caption))
      @$el.html(t())

      @$el

    destroy: () ->
      @$el.empty()
      delete @el

  RibbonView