define [
  'underscore'
  'jquery'
  'tools/Mediator'

  'book/BookPageModel'
  'domain/ItemModel'
  'domain/CategoryCollection'
  'book/BookModel'
  'item/ItemController'
  'text!/assets/js/full/book/PageTemplate.html'

], (_, $, Mediator, BookPageModel, ItemModel, CategoryCollection, BookModel, ItemController, PageTemplate) ->
  class BookController
    bookPage: null
    book: null
    prevPageOffset: -1
    nextPageOffset: -1
    itemController: null;
    categoryCollection: null;


    constructor: (@container, @bookId) ->
      _.bindAll(this, "reloadPage",
        "showPage", "translate", "save", "failToSave", "init", "success",
        "showNextPage", "showPrevPage", "initBook", "hideAddItemWidget", "initModel");
      @subscribe()
      @initModel()

    getRoute: () ->
      "book"

    initModel: () ->
      @categoryCollection = new CategoryCollection()
      @categoryCollection.fetch()

    showNextPage: () ->
      @book.set('offset', @nextPageOffset)
      @book.save({}, {success: @reloadPage})

    showPrevPage: () ->
      @book.set('offset', @prevPageOffset)
      @book.save({}, {success: @reloadPage})

    init: () ->
      t = _.template(PageTemplate)
      @container.html(t())
      @container.find('#front').change( @updateAll )
      @container.find('#save').click ( @save )
      @container.find('#nextPage').click( @showNextPage )
      @container.find('#nextPage').attr("disabled", "true")
      @container.find('#prevPage').click( @showPrevPage )
      @container.find('#prevPage').attr("disabled", "true")
      @container.find('#discard').click ( @hideAddItemWidget )

      @hideAddItemWidget()
      @bookPage = new BookPageModel()
      @bookPage.url = '/bookPage/' + @bookId
      @reloadPage()

    reloadPage: () ->
      @bookPage.fetch({ success: @initBook })

    initBook: () ->
      @book = new BookModel()
      @book.url = '/book/' + @bookId
      @book.fetch({ success: @showPage })

    initPagination:() ->
      @prevPageOffset = @bookPage.get('prevPageOffset')
      @nextPageOffset = @bookPage.get('nextPageOffset')

      if (@nextPageOffset == -1)
        @container.find('#nextPage').attr("disabled", true)
      else
        @container.find('#nextPage').attr("disabled", false)

      if (@prevPageOffset == -1)
        @container.find('#prevPage').attr("disabled", true)
      else
        @container.find('#prevPage').attr("disabled", false)

    showPage: () ->
      @initPagination()
      @container.find('#book_content').html(@bookPage.get('content'))
      @container.find('#book_content').find('trnsl').click( @translate )

    translate: (source) ->
      @itemController = new ItemController(@container.find('#create_item_container'))
      @itemController.init(@categoryCollection)
      word = $(source.target).text().toLowerCase()
      @itemController.setText(word)

    save: () ->
      @model = new ItemModel()
      @model.url = '/item'
      @model.set('front', {'value': @itemController.getFront(), 'contentType': 'TextType'})
      @model.set('categoryId', $('#category').val())
      @model.set('back', {'value': @itemController.getBack(), 'contentType': 'TextType'})
      @model.set('desc', {'value': @itemController.getDefinition(), 'contentType': 'TextType'})
      @model.save({}, { success: @success , error: @failToSave })

    failToSave: (result) ->
      @hideAddItemWidget()
      window.alert('ERROR: item with front value \'' + result.get('front').value + '\' allready exists')

    success: () ->
      @book.get('items').push(@model.get('id'))
      @book.save({}, {success: @reloadPage})
      @hideAddItemWidget()

    hideAddItemWidget: () ->
      if (@itemController != null)
        @itemController.destroy()
        delete @itemController

    unsubscribe: () ->
      Mediator.unsubscribe('save_word')
      Mediator.unsubscribe('cancel_save_word')

    subscribe: () ->
      Mediator.subscribe('save_word', @save, @)
      Mediator.subscribe('cancel_save_word', @hideAddItemWidget, @)

    destroy: () ->
      @hideAddItemWidget()
      @unsubscribe

  return BookController