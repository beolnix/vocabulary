define [

  # Libs
  'backbone'

], (Backbone) ->
  BookModel = Backbone.Model.extend
    url: '/book'
  BookModel