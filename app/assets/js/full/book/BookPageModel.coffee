define [

  # Libs
  'backbone'

], (Backbone) ->
  BookPageModel = Backbone.Model.extend
    url: '/bookPage'
  BookPageModel