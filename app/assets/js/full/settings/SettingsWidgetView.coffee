define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'

  #template
  'text!./SettingsWidgetTemplate.html'

], (Backbone, $, _, Mediator, settingsTemplate) ->
  SettingsWidgetView = Backbone.View.extend
    tagName: "div"
    className: "header_container"
    settingsActivator: null
    settingsHeight: null
    settingsHided: true

    init: (@model) ->
      _.bindAll(this, "toggleSettings", "render", "showHeader", "calculateActualHeight");

    render: () ->
      t = _.template(settingsTemplate)
      @$el.html(t())
      @$el

    calculateActualHeight: () ->
      @settingsHeight = 0

    toggleSettings: () ->
      operator = '-='
      if (@settingsHided)
        operator = '+='
        @settingsHided = false
      else
        @settingsHided = true
      animateParams =
        'padding-top': operator + @settingsHeight;
      @$el.animate(animateParams, 500)

    showHeader: () ->
      @calculateActualHeight()
      animateParams =
        'margin-top': @settingsHeight * -1;
      @$el.animate(animateParams, 500)


  return SettingsWidgetView