define [
  'underscore'
  'jquery'

  'settings/SettingsWidgetView'

], (_, $, SettingsWidgetView) ->
  SettingsWidgetController =
    settingsWidgetView: null

    init: () ->
      _.bindAll(this, "showHeader");
      @settingsWidgetView = new SettingsWidgetView()
      @settingsWidgetView.init()
      $('body').prepend(@settingsWidgetView.render())

    showHeader: () ->
      @settingsWidgetView.showHeader()

  SettingsWidgetController