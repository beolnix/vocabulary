requirejs.config

    deps: ['main']

    paths:
        'text': '/assets/js/libs/rjs-text'
        'jquery': '/assets/js/libs/jquery-2.1.0.min'
        'backbone': '/assets/js/libs/backbone'
        'underscore': '/assets/js/libs/underscore'
        'less': '/assets/js/libs/less'
        'customFlip': '/assets/js/libs/customFlip'
        'chart': '/assets/js/libs/chart'

    shim:
        'backbone':
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        'customFlip':
            deps: ['jquery']
            exports: '$'
        'underscore':
            exports: '_'

    

	



