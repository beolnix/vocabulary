define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'

  #template
  'text!./ShowHideExTemplate.html'

], (Backbone, $, _, Mediator, ShowHideExTemplate) ->
  ShowHideExView = Backbone.View.extend
    tagName: "div"

    initialize: (@exAll, @exCurrent, @stageName, @showContent, @hideContent, @hint) ->
      _.bindAll(this, "showHidden", "sendSuccessMessage", "sendFailMessage");
      #

    render: () ->
      params =
        stagename: @stageName
        exAll: @exAll
        exCurrent: @exCurrent
        hint: @hint
        front: @getContentForItem(@showContent)
        back: @getContentForItem(@hideContent)

      t = _.template(ShowHideExTemplate)
      @$el.html(t(params))

      @$el.find('#the_button').click( @showHidden )
      @$el.find('#success_button').click( @sendSuccessMessage )
      @$el.find('#fail_button').click( @sendFailMessage )

      @$el

    sendSuccessMessage: () ->
      Mediator.publish('trainItemResult', 'success')

    sendFailMessage: () ->
      Mediator.publish('trainItemResult', 'fail')

    getContentForItem: (itemContent) ->
      content = null
      if (itemContent.contentType == "TextType")
        content = itemContent.value
      else
        window.alert('type ' + itemContent.contentType + ' not implemented yet!')
      content

    showHidden: () ->
      @$el.find('#the_button').addClass('hidden')
      @$el.find('#hint').addClass('hidden')
      @$el.find('#back').removeClass('hidden')
      @$el.find('#hiding_buttons').removeClass('hidden')

  ShowHideExView