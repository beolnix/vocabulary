define [
  'underscore'
  'jquery'
  'tools/Mediator'

  'domain/TrainingInfoModel'
  'domain/ItemModel'
  'train/ShowHideExView'


], (_, $, Mediator, TrainingInfoModel, ItemModel, ShowHideExView) ->
  class TrainingController

    trainingInfo: null
    currentItemIndex: null
    currentExerciseIndex: null
    item: null
    view: null

    constructor: (@container, @trainId) ->
      #

    init: () ->
      _.bindAll(this, "showNextItem", "processTrainResult", "getNewItem", "updateItems");
      @subscribe()
      @trainingInfo = new TrainingInfoModel()
      @trainingInfo.url = '/trainingInfo/' + @trainId
      @currentItemIndex = 0
      @trainingInfo.fetch({success: @showNextItem})

    getRoute: () ->
      "train/" + @trainId

    showNextItem: () ->
      @prepareNextItem()
      exercise = @getNextExercise()
      @container.empty()

      if (exercise.exType == "ShowFrontHideBack")
        @displayShowHideExercise(@item.get('front'), @item.get('back'), @item, "the translation is ...")
      else if (exercise.exType == "ShowBackHideFront")
        @displayShowHideExercise(@item.get('back'), @item.get('front'), @item, "the word is ...")
      else if (exercise.exType == "ShowDescHideBack" || exercise.exType == "ShowDescHideFront")
        @displayShowHideExercise(@item.get('desc'), @item.get('front'), @item, "the word is ...")
      else if (exercise.exType == "ShowBackTypeFront")
        @displayShowInputExercise(@item.get('back'), @item.get('front'), @item)
      else
        window.alert('exercise ' + exercise.exType + ' is not implemented yet!')

      #@view = new ShowHideExView( @trainingInfo.get('category'), @trainingInfo.get('items')[@currentItemIndex], @currentExerciseIndex)
      #@container.empty()
      #@container.html(@view.render())


    prepareNextItem: () ->
      @item = new ItemModel(@trainingInfo.get('items')[@currentItemIndex])
      @item.url = @item.url + '/' + @item.id

    getNextExercise: () ->
      @exerciseList = @getExerciseList(@item)
      if (@item.get('currExNum') == undefined)
        @item.set('currExNum', 0)
      @exerciseList[@item.get('currExNum')]

    getExerciseList: (item) ->
      if (item.get('trainingProgress') is null)
        @trainingInfo.get('category').currExs
      else if (item.get('trainingProgress').stage == "New")
        @trainingInfo.get('category').currExs
      else if (item.get('trainingProgress').stage == "Practise")
        @trainingInfo.get('category').currExs
      else
        @trainingInfo.get('category').reExs


    displayShowInputExercise: (showContent, inputCheckContent, item) ->
      #@view = new ShowInputExView(showContent, inputCheckContent, item)
      #@container.html(@view.render())

    displayShowHideExercise: (showContent, hideContent, item, hint) ->
      @view = new ShowHideExView(@getExerciseList(item).length, item.get('currExNum'), item.get('trainingProgress').stage, showContent, hideContent, hint)
      @container.html(@view.render())


    processTrainResult: (result) ->
      if (result == 'success')
        currExNum = @item.get('currExNum') + 1
        @item.set('currExNum', currExNum)
      else
        @item.set('currExNum', 0)

      if (@item.get('currExNum') >= @exerciseList.length)
        @finishTrainItem()
      else
        @trainingInfo.get('items')[@currentItemIndex] = @item
        @finishCurrentIteration()
        @showNextItem()


    finishTrainItem: () ->
      if (@item.get('trainingProgress').stage == 'Repeat')
        @item.get('trainingProgress').stage = 'Practise'
        @item.get('trainingProgress').repeatDt = new Date().getTime()
      else if (@item.get('trainingProgress').stage == 'Practise')
        newPeiodNum = @item.get('trainingProgress').periodNum + 1
        @item.get('trainingProgress').periodNum = newPeiodNum
        period = @getNextPeriod(newPeiodNum)
        startDate = new Date()
        startDate.setDate(startDate.getDate() + period.periodVal)
        @item.get('trainingProgress').periodStartDt = startDate.getTime()
      else if (@item.get('trainingProgress').stage == 'New')
        @item.get('trainingProgress').stage = 'Practise'
        newPeriodNum = @item.get('trainingProgress').periodNum + 1
        @item.get('trainingProgress').periodNum = newPeriodNum
        period = @getNextPeriod(newPeriodNum)
        startDate = new Date()
        startDate.setDate(startDate.getDate() + period.periodVal)
        @item.get('trainingProgress').periodStartDt = startDate.getTime()
      else
        window.alert('stage ' + @item.get('trainingProgress').stage + ' not implemented yet!')
      @item.save({}, {success: @getNewItem })

    getNextPeriod: (periodIndex) ->
      periodList = @trainingInfo.get('category').periods
      if (periodIndex >= periodList.length)
        periodList[periodList.length - 1]
      else
        periodList[periodIndex]

    getNewItem: () ->
      @newItem = new ItemModel
      ids = ""
      comma = ""
      for item in @trainingInfo.get('items')
        do (item) ->
          ids += comma + item.id
          comma = ","
      @newItem.url = '/trainWord/' + @trainingInfo.get('category').id + '?exept=' + ids + '&' + 'newCount=' + @getNewItemNum()
      @newItem.fetch({success: @updateItems})

    getNewItemNum: () ->
      count = 0
      for item in @trainingInfo.get('items')
        do (item) ->
          if (item.get('trainingProgress').stage == 'New')
            count += 1

      count

    updateItems: () ->
      @newItem.url = '/item/' + @newItem.get('id')
      @trainingInfo.get('items')[@currentItemIndex] = @newItem
      @showNextItem()


    subscribe: () ->
      Mediator.subscribe('trainItemResult', @processTrainResult, @)

    finishCurrentIteration: () ->
      @currentItemIndex += 1

      if (@currentItemIndex >= @trainingInfo.get('items').length)
        @currentItemIndex = 0



    destroy: () ->
      #



  TrainingController