requirejs [

  # Libs
  'backbone'
  'jquery',
  'VocabularyController'
  'VocabularyRouter'

], (Backbone, $, VocabularyController, VocabularyRouter) ->


  vocabulryController = new VocabularyController()
  vocabularyRouter = new VocabularyRouter()

  vocabulryController.init(vocabularyRouter)
  vocabularyRouter.init(vocabulryController)
  Backbone.history.start()

  if (window.location.hash == "")
    vocabulryController.showCategories("all")
  else
    vocabularyRouter.navigate(window.location.hash.replace("#", ""), true)


