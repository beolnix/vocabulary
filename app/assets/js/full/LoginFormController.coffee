define [
  'underscore'
  'jquery'

  'tools/Mediator'

  'RibbonView'
  'LoginFormView'

  'text!/assets/js/full/LoginFormTemplate.html'
], (_, $, Mediator, RibbonView, LoginFormView, loginFormTemplate) ->
  class LoginFormController
    ribbonView: new RibbonView()
    loginFormView: new LoginFormView()
    callback: null

    constructor: (@container) ->
      #

    init: () ->
      _.bindAll(this, "loginAction", "loginSuccess", "loginFail");
      @subscribe()
      @container.empty()
      @ribbonView.init('welcome!')
      @loginFormView.init()
      @showLoginForm()

    getRoute: () ->
      "login"

    showLoginForm: () ->
      @container.empty()
      @container.append(@ribbonView.render())
      @container.append(@loginFormView.render())

    subscribe: () ->
      Mediator.subscribe('login', @loginAction, @)

    unsubscribe: () ->
      Mediator.unsubscribe('login')

    loginAction: (login, password) ->
      $.get('/restLogin?login=' + login + '&pswd=' + password)
        .done( @loginSuccess )
        .fail( @loginFail )


    loginSuccess: (data) ->
      Mediator.publish('loginSuccess')

    loginFail: (data) ->
      window.alert('wrong login or password')

    destroy: () ->
      @unsubscribe()
      if (!(@ribbonView is undefined))
        @ribbonView.destroy()
        delete @ribbonView
      if (!(@loginFormView is undefined))
        @loginFormView.destroy()
        delete @loginFormView