define [

    # Libs
    'backbone'

], (Backbone) ->
    Mediator = {}

    Mediator.subscribe = Backbone.Events.on
    Mediator.unsubscribe = Backbone.Events.off
    Mediator.publish = Backbone.Events.trigger

     #    	events: []
    	# subscribe: (eventName, method, context) ->
    	# 	@events.push 
    	# 		name: eventName
    	# 		call: method
    	# 		ctx: context

    	# publish: (eventName, params) ->
    	# 	_.each @events,  (event) ->
    	# 		if (event.name == eventName)
    	# 			if (params == 0)
    	# 				event.call.apply(event.ctx)
    	# 			else
    	# 				event.call.call(event.ctx, params)

    	# unsubscribe: (eventName) ->
    	# 	_.each @events, (event) ->
    	# 		if (event.name == eventName)
    	# 			delete event 

    Mediator