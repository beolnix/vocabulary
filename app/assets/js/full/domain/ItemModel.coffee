define [

  # Libs
  'backbone'

], (Backbone) ->
  ItemModel = Backbone.Model.extend
    defaults:
      progress: { periodNum: 0, repeatDt: new Date() }
    url: '/rest/item'
  ItemModel