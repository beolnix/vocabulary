define [
  # Lib
  'backbone'

  #Deps
  './ItemModel'
], (Backbone, ItemModel) ->
  ItemCollection = Backbone.Collection.extend
    model: ItemModel,
    url: '/rest/item'
  ItemCollection