define [
  # Lib
  'backbone'

  #Deps
  './CategoryModel'
], (Backbone, CategoryModel) ->
  CategoryCollection = Backbone.Collection.extend
    model: CategoryModel,
    url: '/rest/category'
  CategoryCollection