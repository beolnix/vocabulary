define [

  # Libs
  'backbone'

], (Backbone) ->

  CategoryModel = Backbone.Model.extend
    defaults:
      name: 'default',
      currExs: [ { orderNum: 1, exType: 'ShowFrontHideBack' } ,
                 { orderNum: 2, exType: 'ShowBackHideFront' } ,
                 { orderNum: 3, exType: 'ShowDescHideBack' } ],
      periods: [ { orderNum: 1, periodVal: 1 },
                 { orderNum: 2, periodVal: 2 },
                 { orderNum: 3, periodVal: 4 } ],

      repeatMax: 5,
      currentMax: 5,
      reExs: [ { orderNum: 1, exType: 'ShowFrontHideBack' } ,
               { orderNum: 2, exType: 'ShowBackHideFront' } ,
               { orderNum: 3, exType: 'ShowDescHideBack' } ]
    url: '/rest/category'

  CategoryModel