define [

  # Libs
  'backbone'

], (Backbone) ->
  StatisticsModel = Backbone.Model.extend
    url: '/rest/statistics'
  StatisticsModel