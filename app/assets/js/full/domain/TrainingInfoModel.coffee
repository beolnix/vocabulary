define [

  # Libs
  'backbone'

], (Backbone) ->
  TrainingModel = Backbone.Model.extend
    url: '/rest/trainingInfo'
  TrainingModel