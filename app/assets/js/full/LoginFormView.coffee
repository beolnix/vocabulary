define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  'tools/Mediator'

  #template
  'text!/assets/js/full/LoginFormTemplate.html'

], (Backbone, $, _, Mediator, loginFormTemplate) ->
  LoginFormView = Backbone.View.extend
    tagName: "div"

    init: () ->
      _.bindAll(this, "login");

    render: () ->
      t = _.template(loginFormTemplate)
      @$el.html(t())
      @$el.find('#loginBtn').click( @login )
      @$el

    login: () ->
      login = @$el.find('#loginInput').val()
      pswd = @$el.find('#pswdInput').val()
      Mediator.publish('login', login, pswd)

    destroy: () ->
      @$el.empty()
      delete @el

  LoginFormView