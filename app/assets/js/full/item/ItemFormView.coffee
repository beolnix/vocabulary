define [
  'underscore'
  'jquery'
  'tools/Mediator'

  './CategorySelectView'
  'categories/CreateCategoryEmbeddedFormView'

  'text!./ItemFormTemplate.html'
], (_, $, Mediator, CategorySelectView, CreateCategoryEmbeddedFormView, ItemFormTemplate) ->
  ItemFormView = Backbone.View.extend
    categorySelectView: null
    createCategoryEmbeddedFormView: null
    newCategoryRequired: false
    events:
      "click #saveBtn": "save"
      "click #cancelBtn": "cancel"

    initialize: () ->
      _.bindAll(this, "render", "setText", "updateAll", "updateTranslate", "updateDesc", "save", "cancel")
      @subscribe()

    subscribe: () ->
      #Mediator.subscribe('item_create_form.new_category_selected', @createNewCategorySelected)
      #Mediator.subscribe('item_create_form.cancel_create_new_category', @cancelCreateNewCategory)

    unsubscribe: () ->
      #Mediator.unsubscribe('item_create_form.new_category_selected')
      #Mediator.unsubscribe('item_create_form.cancel_create_new_category')

    createNewCategorySelected: () ->
      @hideCategorySelectView()
      @showCreateNewCategory()

    cancelCreateNewCategory: () ->
      @hideCreateNewCategory()
      @showCategorySelectView()

    showCreateNewCategory: () ->
      @hideCreateNewCategory()
      @newCategoryRequired = true
      @createCategoryEmbeddedFormView = new CreateCategoryEmbeddedFormView
      @$el.find('#select_category_container').html(@createCategoryEmbeddedFormView.render())

    hideCreateNewCategory: () ->
      @newCategoryRequired = false

      if (@createCategoryEmbeddedFormView)
        @createCategoryEmbeddedFormView.destroy()
        delete @createCategoryEmbeddedFormView

    showCategorySelectView: () ->
      @hideCategorySelectView()
      @categorySelectView = new CategorySelectView(model: @model.models)
      @$el.find('#select_category_container').html(@categorySelectView.render())

    hideCategorySelectView: () ->
      if(@categorySelectView)
        @categorySelectView.destroy()
        delete @categorySelectView
      delete @categorySelectView

    render: () ->
      t = _.template(ItemFormTemplate)
      @$el.html(t())
      @showCategorySelectView()

      return @$el

    setText: (text) ->
      @$el.find("#frontInput").val(text)
      @updateAll()

    getCategoryId: () ->
      if (@categorySelectView)
        @categorySelectView.getSelectedCategoryId()
      else
        null

    getNewCategoryName: () ->
      if (@createCategoryEmbeddedFormView)
        @createCategoryEmbeddedFormView.getCategoryName()
      else
        null

    getFront: () ->
      @$el.find("#frontInput").val()

    getBack: () ->
      @$el.find("#backInput").val()

    getDefinition: () ->
      @$el.find("#definitionInput").val()

    getContext: () ->
      @$el.find("#contextInput").val()

    updateAll: () ->
      @updateDesc()
      @updateTranslate()

    isNewCategoryRequired: () ->
      @newCategoryRequired

    updateTranslate: () ->
      $.get('https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20130425T132859Z.980d0a6b109bd6e8.46c32225dc13898b35efdecc834f4fa62a95fe0a&lang=en-ru&text=' + $('#frontInput').val(), (data) ->
        translate = data.text[0]
        while (translate.indexOf("\"") != -1)
          translate = translate.replace("\"", "")

        $('#backInput').val(translate.toLowerCase())
      )

    updateDesc: () ->
      $.get('/findDesc/' + $('#front').val(), (data) ->
        $('#definitionInput').val(data.value.value)
      )

    save: () ->
      Mediator.publish('save_item_btn_pressed')

    cancel: () ->
      Mediator.publish('cancel_create_item_btn_pressed')

    destroy: () ->
      @unsubscribe()
      $(elem).attr('onclick','').unbind('click') for elem in @$el.find('#create_item_form-categories_list').children()
      @$el.remove()

  return ItemFormView


