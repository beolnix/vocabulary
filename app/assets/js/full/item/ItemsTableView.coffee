define [
  'underscore'
  'jquery'
  'tools/Mediator'

  'item/ItemView'

  'text!./ItemsTableTemplate.html'
], (_, $, Mediator, ItemView, ItemsTableTemplate) ->
  ItemsTableView = Backbone.View.extend
    tagName: "div"
    itemViews: []

    init: (@itemCollection) ->
      _.bindAll(this, "render")

    render: () ->
      t = _.template(ItemsTableTemplate)
      @$el.html(t())
      itemViews: []

      for item in @itemCollection.models
        itemView = new ItemView
        itemView.init(item)
        @itemView += itemView
        @$el.find('#items_table_container').append(itemView.render())

      return @$el

    destroy: () ->
#      @$el.find('#saveBtn').attr('onclick','').unbind('click')
#      @$el.find('#cancelBtn').attr('onclick','').unbind('click')
      @$el.remove()
      delete @el

  ItemsTableView