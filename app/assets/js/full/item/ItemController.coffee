define [
  'underscore'
  'jquery'
  'tools/Mediator'

  'item/ItemFormView'
], (_, $, Mediator, ItemFormView) ->
  class ItemController
    itemFormView: null
    categoryCollection: null

    constructor: (@container) ->
      #

    init: (@categories) ->
      _.bindAll(this, "setText")
      @subscribe()
      @itemFormView = new ItemFormView()
      @itemFormView.init(@categories)
      @container.html(@itemFormView.render())

    setText: (text) ->
      @itemFormView.setText(text)

    getRoute: () ->
      "createItem"

    getFront: () ->
      return @itemFormView.getFront()

    getBack: () ->
      return @itemFormView.getBack()

    getDefinition: () ->
      return @itemFormView.getDefinition()

    subscribe: () ->
      #

    unsubscribe: () ->
      #

    destroy: () ->
      @unsubscribe()
      if (@itemFormView != null)
        @itemFormView.destroy()
        delete @itemFormView

  return ItemController


