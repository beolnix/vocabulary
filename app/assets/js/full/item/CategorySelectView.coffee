define [
  'underscore'
  'jquery'
  'tools/Mediator'

  'text!./SelectCategoryView.html'
], (_, $, Mediator, ItemsTableTemplate) ->
  CategorySelectView = Backbone.View.extend
    selectedCategoryElemId: null
    selectedCategoryName: null

    initialize: () ->
      _.bindAll(this, "selectCategory")

    render: () ->
      t = _.template(ItemsTableTemplate)
      @$el.html(t(categories: @model))
      $(elem).click(@selectCategory) for elem in @$el.find('#create_item_form-categories_list').children()
      @$el

    selectCategory: (source) ->
      @selectedCategoryElemId = $(source.target).data().categoryId
      @selectedCategoryName = $(source.target).text()
      if (@selectedCategoryElemId == "new")
        Mediator.publish("item_create_form.new_category_selected")
      else
        @$el.find("#create_item-select_category_menu").html("<b>" + @selectedCategoryName + "</b>")

    getSelectedCategoryId: () ->
      @selectedCategoryElemId

    getSelectedCategoryName: () ->
      @selectedCategoryName

    destroy: () ->
      $(elem).attr('onclick','').unbind('click') for elem in @$el.find('#create_item_form-categories_list').children()

  CategorySelectView