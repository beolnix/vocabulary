define [
  'underscore'
  'jquery'
  'tools/Mediator'

  'text!./ItemTemplate.html'
], (_, $, Mediator, ItemTemplate) ->
  ItemView = Backbone.View.extend
    tagName: "tr"

    init: (@item) ->
      _.bindAll(this, "render")

    render: () ->
      t = _.template(ItemTemplate)
      params =
        front: @item.get('front').value
        back: @item.get('back').value
        periodNum: @item.get('progress').periodNum
        periodDt: @item.get('progress').repeatDt

      @$el.html(t(params))
      return @$el

    destroy: () ->
      @$el.remove()
      delete @el

  ItemView
