define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'

  #template
  'text!./CreateCategoryButtonTemplate.html'

], (Backbone, $, _, Mediator, categoryButtonTemplate) ->
  CreateCategoryButtonView = Backbone.View.extend
    tagName: "div"
    events:
      "click #create_category_button": "createCat"
      "click #create_item_button": 'createItemButtonPressed'

    initialize: () ->
      @$el

    render: () ->
      t = _.template(categoryButtonTemplate)
      @$el.html(t())
      return @$el

    createCat: () ->
      Mediator.publish('create_category_btn_pressed')

    createItemButtonPressed: () ->
      Mediator.publish("create_item_btn_pressed")

    destroy: () ->
      #

  CreateCategoryButtonView