define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'

  'text!./ItemsControlsTemplate.html'

], (Backbone, $, _, Mediator, ItemsControllerTemplate) ->
  ItemsControllerView = Backbone.View.extend
    tagName: 'div'

    init: (@category) ->
      _.bindAll(this, "render", "destroy")

    render: () ->
      params =
        category: @category
      t = _.template(ItemsControllerTemplate)
      @$el.html(t(params))
      return @$el

    destroy: () ->
      @$el.remove()
      delete @$el



  ItemsControllerView