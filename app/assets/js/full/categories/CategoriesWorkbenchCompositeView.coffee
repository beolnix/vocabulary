define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'

  'categories/ListView'
  'categories/ItemControlsView'
  'categories/CreateCategoryButtonView'
  'categories/CreateCategoryFormView'
  'categories/StatAgendaView'
  'domain/CategoryModel'

  'domain/ItemModel'
  'domain/StatisticsModel'

  'item/ItemsTableView'
  'item/ItemFormView'
  'categories/GlobalStat'


  'text!./CategoriesWorkbenchCompositeTemplate.html'
  'text!./CategoriesListTemplate.html'
  'text!./MiniCategoriesListTemplate.html'

], (Backbone, $, _, Mediator, ListView, ItemControlView, CreateCategoryButtonView,
    CreateCategoryFormView, StatAgendaView, CategoryModel, ItemModel, StatisticsModel, ItemsTableView, ItemFormView, GlobalStat, Template, ListViewTemplate, MiniListTemplate) ->
  CategoriesWorkbenchCompositeView = Backbone.View.extend
    tagName: 'div'

    statistics: null

    itemsControlView: null
    itemsTableView: null

    categoriesListView: null
    #categoriesMiniListView: null

    createItemFormView: null
    createCategoryBtnView: null
    createCategoryFormView: null
    statAgendaView: null

    chartLg: null
    chartMd: null

    selectedCategory: null

    initialize: () ->
      #_.bindAll(@, )
      @subscribe()

    subscribe: () ->
      Mediator.subscribe("categories:item_form:create_item_btn:pressed", @createItemBtnPressed, @)
      Mediator.subscribe("categories:item_form:cancel_create_item_btn:pressed", @cancelCreateItemBtnPressed, @)
      Mediator.subscribe("categories:item_form:save_item_btn:pressed", @saveItemBtnPressed, @)

      Mediator.subscribe("categories:category_form:create_category_btn:pressed", @createCategoryBtnPressed, @)
      Mediator.subscribe("categories:category_form:cancel_create_category_btn:pressed", @cancelCreateCategoryBtnPressed, @)
      Mediator.subscribe("categories:category_form:save_category_btn:pressed", @saveCategoryBtnPressed, @)

      Mediator.subscribe('categories:item_form:item_create_form:new_category_selected', @showCreateCategoryForm(), @)

    unsubscribe: () ->
      Mediator.unsubscribe(null, null, @)

    saveItemValidationError: (model, errorObj) ->


    saveItemError: (model, errorObj) ->

    cancelCreateNewCategoryPressed: () ->

    saveNewCategoryPressed: () ->


    createCategoryBtnPressed: () ->
      @hideCreateCategoryBtn()
      @showCreateCategoryForm()

    cancelCreateCategoryBtnPressed: () ->
      @hideCreateCategoryForm()
      @showCreateCategoryBtn()

    saveCategoryBtnPressed: () ->
      catName = @createCategoryFormView.getCategoryName()
      categoryModel = new CategoryModel
        name: catName
      Mediator.publish('save_category', categoryModel)

      @hideCreateCategoryForm()
      @showCreateCategoryBtn()

    createItemBtnPressed: () ->
      @hideItemsTable()
      @hideItemsControlView()
      @hideCategoriesMiniListView()
      @showCreateItemForm()

    cancelCreateItemBtnPressed: () ->
      @hideCreateItemForm()
      @showItemsControlView()
      @showItemsTable()
      @showCategoriesMiniListView()

    saveItemBtnPressed: () ->
      if (@createItemFormView.isNewCategoryRequired())
        catName = @createItemFormView.getNewCategoryName()
        categoryModel = new CategoryModel
          name: catName
        _self = this
        categoryModel.save({}, {success: _self.saveItemWithCategoryModel, error: _self.saveItemWithCategoryModel})
      else
        @saveItem(@createItemFormView.getCategoryId())

    saveItemWithCategoryModel: (categoryModel) ->
      Mediator.publish("reload_categories_list")
      @saveItem(categoryModel.attributes["id"])

    saveItem: (categoryId) ->
      model = new ItemModel
        ctx: { value: @createItemFormView.getContext(), cType: "text" }
        front: { value: @createItemFormView.getFront(), cType: "text" }
        back: { value: @createItemFormView.getBack(), cType: "text" }
        desc: { value: @createItemFormView.getDefinition(), cType: "text" }
        catId: categoryId

      Mediator.publish('save_item', model)



    render: () ->
      t = _.template(Template)
      @$el.html(t())
      @showCreateCategoryBtn()
      @showItemsControlView()
      return @$el

    updateCategories: (@categories) ->
      @showCagetoriesListView()
      #@showCategoriesMiniListView()

    updateStatistics: () ->
      if (@statistics)
        delete @statistics

      @statistics = new StatisticsModel()
      @statistics.fetch( { success: @updateStatisticsViews } )

    updateItems: (@items, @selectedCategory) ->
      @showItemsTable()
      @showItemsControlView()

    updateStatisticsViews: () ->
      @categoriesListView.updateStatistics(@statistics)
      #@categoriesMiniListView.updateStatistics(@statistics)
      @showStatAgentView()

    showStatAgentView: () ->
      @hideStatAgentView()
      if (@chartLg)
        delete @chartLg
      @chartLg = new GlobalStat(235, 250)

      if (@chartMd)
        delete @chartMd
      @chartMd = new GlobalStat(180, 225)

      @chartLg.init(@$el.find('#canvas-lg').get(0), @statistics)
      @chartMd.init(@$el.find('#canvas-md').get(0), @statistics)
      @statAgendaView = new StatAgendaView(model: @statistics)
      @$el.find('#stat_agenda_container').html(@statAgendaView.render())

    hideStatAgentView: () ->
      if (@statAgendaView)
        @statAgendaView.remove()
        @statAgendaView.destroy()
        delete @statAgendaView

    showCreateItemForm: () ->
      @hideCreateItemForm()
      @hideCreateCategoryForm()
      @hideItemsControlView()
      @hideItemsTable()
      @createItemFormView = new ItemFormView(model: @categories)
      @$el.find("#create_item_container").html(@createItemFormView.render())

    hideCreateItemForm: () ->
      if (@createItemFormView)
        @createItemFormView.destroy()
        delete @createItemFormView

    showItemsTable: () ->
      @hideItemsTable()
      @itemsTableView = new ItemsTableView
      @itemsTableView.init(@items)
      @$el.find("#items_table_container").html(@itemsTableView.render())

    hideItemsTable: () ->
      if (@itemsTableView)
        @itemsTableView.destroy()
        delete @itemsTableView

    hideCreateCategoryBtn: () ->
      if (@createCategoryBtnView)
        @createCategoryBtnView.remove()
        delete @createCategoryBtnView

    showCreateCategoryBtn: () ->
      @hideCreateCategoryBtn()
      @createCategoryBtnView = new CreateCategoryButtonView
      @$el.find('#create_category_button_container').html(@createCategoryBtnView.render())

    showCreateCategoryForm: () ->
      @hideCreateCategoryForm()
      @hideCreateItemForm()
      @hideItemsTable()
      @hideItemsControlView()
      @hideCreateItemForm()
      @createCategoryFormView = new CreateCategoryFormView()
      @$el.find('#create_cagetory_container').html(@createCategoryFormView.render())
#      @hideCreateCategoryBtn()

    hideCreateCategoryForm: () ->
      if (@createCategoryFormView)
        @createCategoryFormView.destroy()
        @createCategoryFormView.remove()
        delete @createCategoryFormView
      @showCreateCategoryBtn()
      @showItemsTable()
      @showItemsControlView()

    showItemsControlView: () ->
      @hideItemsControlView()
      @itemsControlView = new ItemControlView
      @itemsControlView.init(@category)
      @$el.find("#item_controls_container").html(@itemsControlView.render())


    hideItemsControlView: () ->
      if (@itemsControlView)
        @itemsControlView.destroy()
        delete @itemsControlView


    showCategoriesMiniListView: () ->
      @hideCategoriesMiniListView()
      @categoriesMiniListView = new ListView
      @categoriesMiniListView.init(@categories, MiniListTemplate, '#mini_categories_menu', 'mini')
      @$el.find('#mini_categories_menu_container').html(@categoriesMiniListView.render())

    showCagetoriesListView: () ->
      @hideCategoriesListView()
      @categoriesListView = new ListView
      @categoriesListView.init(@categories, ListViewTemplate, '#categories_list', 'full', @category)
      @$el.find('#categories_menu_container').html(@categoriesListView.render())


    hideCategoriesListView:() ->
      if (@categoriesListView)
        @categoriesListView.destroy()
        delete @categoriesListView

    hideCategoriesMiniListView:() ->
      if (@categoriesMiniListView)
        try
          @categoriesMiniListView.destroy()
        catch error
          #nop
          console.log("same thing")
        @categoriesMiniListView.destroy()
        delete @categoriesMiniListView


    destroy: () ->
      @hideItemsControlView()
      @hideCategoriesListView()
      #@hideCategoriesMiniListView()
      @hideCreateCategoryForm()
      @hideCreateCategoryBtn()
      @unsubscribe()

  CategoriesWorkbenchCompositeView