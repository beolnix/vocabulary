define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'
  'chart'


], (Backbone, $, _, Mediator, Chart) ->
  class GlobalStat

    constructor: (@width, @height) ->

    chartEl: null
    newPercents: 0
    practicePercents: 0
    learnedPercents: 0

    init: (@container, @statistics) ->
      _.bindAll(this, "getData", "updateStat")
      $(@container).attr("width", @width)
      $(@container).attr("height", @height)

      @chartEl = new Chart(@container.getContext("2d")).Doughnut(@getData());

    updateStat: () ->
      allCount = 0
      newCount = 0
      practiseCount = 0
      for catID, category of @statistics.attributes
        allCount += category.all #model.get('counters').all
        newCount += category.onLearn #model.get('counters').new
        practiseCount += category.onRepeat #model.get('counters').practise

      onePercent = 360 / allCount
      @newPercents = newCount * onePercent
      @practicePercents = practiseCount * onePercent
      @learnedPercents = 360 - @newPercents - @practicePercents


    getData: () ->
      @updateStat()
      doughnutData = [
        {
          value : @learnedPercents,
          color : "#46BFBD"
        },
        {
          value: @practicePercents
          color:"#F7464A"
        },
        {
          value : @newPercents,
          color : "#FDB45C"
        }
      ]
      return doughnutData


  return GlobalStat