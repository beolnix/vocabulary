define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'

  #template
  'text!./CreateCategoryEmbeddedFormTemplate.html'

], (Backbone, $, _, Mediator, categoryTemplate) ->
  CreateCategoryEmbeddedFormView = Backbone.View.extend
    tagName: "div"
#    className: "row"
    events:
      "click #createCategoryEmbeddedForm_cancelButton": "closeEmbeddedForm"

    initialize: () ->
      _.bindAll(this, "closeEmbeddedForm")

    render: () ->
      t = _.template(categoryTemplate)
      @$el.html(t())
      return @$el

    closeEmbeddedForm: () ->
      Mediator.publish("item_create_form.cancel_create_new_category")

    getCategoryName: () ->
      @$el.find('#createCategoryEmbeddedForm_catNameInput').val()


    destroy: () ->
      #nop

  CreateCategoryEmbeddedFormView