define [
  'underscore'
  'jquery'
  'tools/Mediator'
  'VocabularyRouter'

  'domain/CategoryModel'
  'domain/CategoryCollection'
  'domain/ItemCollection'
  'domain/StatisticsModel'

  'categories/CategoriesWorkbenchCompositeView'

], (_, $, Mediator, Router, CategoryModel, CategoryCollection, ItemsCollection, StatisticsModel, CategoriesWorkbenchCompositeView) ->
  class CategoryController
    categoryCollection: null
    itemsCollection: null
    statistics: null
    container: null
    categoriesWorkbenchCompositeView: null
    createCategoryFormView: null
    selectedCategory: null

    constructor: (@container, @router, @selectedCategory) ->
      #

    init: () ->
      _.bindAll(this, "updateContent", "createCategory", "hideCategoriesWorkbenchCompositeView",
        "initModel", "saveItem", "updateItemsContent", "updateCategoriesContent", "filterItemsByCategory", "selectedCategoryChanged",
        "successSaveItem", "errorSaveItem", "reloadCategoriesCollection", "clearCategoriesCollection");
      @subscribe()
      @updateContent()

    getRoute: () ->
      "categories/" + @selectedCategory

    clearModel: () ->
      @clearCategoriesCollection()
      @clearItemsCollection()

    clearCategoriesCollection: () ->
      if (@categoryCollection != null)
        @categoryCollection.reset({silent: true})
        delete @categoryCollection

    clearItemsCollection: () ->
      if (@itemsCollection != null)
        @itemsCollection.reset(silent: true)
        delete @itemsCollection

    initModel: () ->
      @clearModel()
      @reloadCategoriesCollection()
      @filterItemsByCategory()

    reloadCategoriesCollection: () ->
      @clearCategoriesCollection()
      @categoryCollection = new CategoryCollection()
      @categoryCollection.fetch( { success: @updateCategoriesContent } )

    filterItemsByCategory: () ->
      if (@itemsCollection)
        @itemsCollection.remove()
        delete @itemsCollection
      @itemsCollection = new ItemsCollection()
      if (@selectedCategory != "all")
        @itemsCollection.url = '/rest/item?catId=' + @selectedCategory

      @itemsCollection.fetch( { success: @updateItemsContent } )

    selectedCategoryChanged: (catId) ->
      if (catId)
        @selectedCategory = catId
      else
        @selectedCategory = "all"
      @filterItemsByCategory()
      @router.navigate("categories/" + @selectedCategory)


    updateContent: () ->
      @showCategoriesWorkbenchCompositeView()

    updateCategoriesContent: () ->
      @categoriesWorkbenchCompositeView.updateCategories(@categoryCollection)
      @categoriesWorkbenchCompositeView.updateStatistics()

    updateItemsContent: () ->
      @categoriesWorkbenchCompositeView.updateItems(@itemsCollection, @selectedCategory)

    subscribe: () ->
      Mediator.subscribe('categories:controller:save_category', @createCategory, @)
      Mediator.subscribe('categories:controller:save_item', @saveItem, @)
      Mediator.subscribe('update_items_list', @selectedCategoryChanged)
      Mediator.subscribe('reload_categories_list', @reloadCategoriesCollection)

    saveItem: (itemModel) ->
      itemModel.save({}, {success: @successSaveItem, error: @errorSaveItem})

    successSaveItem: (savedModel, response, options) ->
      @categoriesWorkbenchCompositeView.hideCreateItemForm()
      @filterItemsByCategory()
      @categoriesWorkbenchCompositeView.updateStatistics()

    errorSaveItem: (model, xhr, options) ->
      if (xhr.status == 400)
        @categoriesWorkbenchCompositeView.saveItemValidationError(model, xhr.responseJSON)
      else
        @categoriesWorkbenchCompositeView.saveItemError(model, xhr.responseJSON)

    createCategory: (categoryModel) ->
      _self = this
      categoryModel.save({}, {success: _self.updateContent, error: _self.updateContent})

    unsubscribe: () ->
      Mediator.unsubscribe('remove_create_category_form')
      Mediator.unsubscribe('show_create_category_form')
      Mediator.unsubscribe('save_category')
      Mediator.unsubscribe('close_create_cat_form')
      Mediator.unsubscribe('save_item')
      Mediator.unsubscribe('reload_categories_list')

    hideCategoriesWorkbenchCompositeView: () ->
      if (@categoriesWorkbenchCompositeView != null)
        @categoriesWorkbenchCompositeView.destroy()
        delete @categoriesWorkbenchCompositeView

    showCategoriesWorkbenchCompositeView: () ->
      @hideCategoriesWorkbenchCompositeView()
      @categoriesWorkbenchCompositeView = new CategoriesWorkbenchCompositeView()
      @categoriesWorkbenchCompositeView.init(@selectedCategory)
      @container.empty()
      @container.html(@categoriesWorkbenchCompositeView.render())
      @initModel()

    destroy: () ->
      @unsubscribe()
      @hideCategoriesWorkbenchCompositeView()
      @container.empty()