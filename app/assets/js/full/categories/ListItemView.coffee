define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'

  #template
  'text!./ListItemTemplate.html'

], (Backbone, $, _, Mediator, ListItemTemplate) ->
  ListItemView = Backbone.View.extend
    tagName: "li"

    init: (@model, @first, @suffix, @statistics) ->
      _.bindAll(this, "deleteCat", "catDeleted", "chooseCategory");
      @$el

    render: () ->
      t = _.template(ListItemTemplate)

      allCount = 0 #@model.get('counters').all
      newCount = 0 #@model.get('counters').new
      practiseCount = 0 #@model.get('counters').practise
      learnedCount = 0 #@model.get('counters').learned
      awaitStatistics = true

      if (@statistics)
        categoryStatistics = this.statistics.attributes[this.model.attributes.id]
        if (categoryStatistics)
          learnedCount = categoryStatistics.onRepeat
          practiseCount = categoryStatistics.onLearn
          allCount = categoryStatistics.all
          awaitStatistics = false

      activeClass = "category_active"
#      activeClass = ""
#      if (allCount > 0)
#        activeClass = "category_active"

      params =
        'id': 'start_training_button_' + @model.get('id') + '_' + @suffix
        'catId': @model.get('id')
        'name': @model.get('name').toUpperCase()
        'practiceCount': practiseCount
        'activeClass': activeClass
        'allCount': allCount
        'awaitStatistics': awaitStatistics
        'firstClass': if @first then 'category_active_first' else ''

      @$el.html(t(params))

      @$el.find('#start_training_button_' + @model.get('id') + '_' + @suffix).click( @chooseCategory )

      return @$el

    chooseCategory: (source) ->
      elem = source.target
      catId = $(elem).data("catId")
      if (!catId)
        elem = source.target.parentNode
        catId = $(elem).data("catId")
      if (!catId)
        elem = source.target.parentNode.parentNode
        catId = $(elem).data("catId")

      elemId = elem.id
      if (elemId == "all")
        elemId = ""

      Mediator.publish('category_changed', elemId, catId)

    deleteCat: () ->
      _self = this
      @model.destroy({success: _self.catDeleted, error: _self.catDeleted})

    catDeleted: () ->
      Mediator.publish('remove_create_category_form')

    destroy: () ->
      @$el.find('#start_training_button_' + @model.get('id') + '_' + @suffix).attr('onclick','').unbind('click');
      @$el.remove()

  return ListItemView