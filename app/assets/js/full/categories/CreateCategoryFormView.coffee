define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'

  #template
  'text!./CreateCategoryFormTemplate.html'

], (Backbone, $, _, Mediator, categoryTemplate) ->
  CreateCategoryFormView = Backbone.View.extend
    tagName: "div"
    events:
      "click #createBtn": "createCat"
      "click #cancelBtn": "cancel"

    render: () ->
      t = _.template(categoryTemplate)
      @$el.html(t())
      return @$el

    createCat: () ->
      Mediator.publish('save_category_btn_pressed')

    getCategoryName: () ->
      @$el.find('#catNameInput').val()

    cancel: () ->
      Mediator.publish('cancel_create_category_btn_pressed')

    destroy: () ->
      #nop

  CreateCategoryFormView