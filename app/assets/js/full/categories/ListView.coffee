define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'

  './ListItemView'

], (Backbone, $, _, Mediator, ListItemView) ->
  ListView = Backbone.View.extend
    viewsCollection: null
    tagName: "div"
    createCategoryBtnView: null

    allCount: 0
    practiseCount: 0
    newCount: 0
    learnedCount: 0
    statistics: null

    selectedCategoryElemId: "start_training_button_all"

    init: (@collection, @template, @placeHolderId, @suffix, @categoryId) ->
      @selectedCategoryElemId = "start_training_button_" + @categoryId + '_' + @suffix
      _.bindAll(this, "render", "destroy", "updateStatistics", "categoryChanged", "updateContent", "chooseCategory")
      @subscribe()

    subscribe: () ->
      Mediator.subscribe("category_changed", @categoryChanged)

    unsubscribe: () ->
      Mediator.unsubscribe("category_changed")

    categoryChanged: (elemId, catId) ->
      @selectedCategoryElemId = elemId
      @updateContent()
      Mediator.publish('update_items_list', catId)

    updateStatistics: (@statistics) ->
      @updateContent()

    updateContent: () ->
      @destroySubViews()
      @render()

    render: () ->
      t = _.template(@template)
      viewContent = t({suffix: @suffix})
      @$el.html(viewContent)
      listPlaceholder = @$el.find(@placeHolderId)
      @viewsCollection = []
      first = true
      @collection.each (model) =>
        model.url = '/rest/category/' + model.get('id')
        view = new ListItemView()
        view.init(model, first, @suffix, @statistics)
        @viewsCollection.push(view)
        content = view.render()
        listPlaceholder.append(content)
        first = false

      @$el.find('#' + @selectedCategoryElemId).addClass('selected')
      @$el.find('#start_training_button_all_' + @suffix).click( @chooseCategory )

      return @$el

    chooseCategory: (source) ->
      catId = $(source.target).data("catId")
      if (catId == "all")
        catId = ""
      elemId = source.target.id
      Mediator.publish('category_changed', elemId, catId)

    destroySubViews: () ->
      if (@viewsCollection.length > 0)
        for catView in @viewsCollection
          do (catView) ->
            catView.destroy()
#            delete catView
        @viewsCollection = []

    destroy: () ->
      @unsubscribe()
      @destroySubViews()

      @$el.remove()

  ListView

