define [

  # Libs
  'backbone'
  'jquery'
  'underscore'

  #deps
  '../tools/Mediator'

  'text!./StatAgendaTemplate.html'

], (Backbone, $, _, Mediator, StatAgendaTemplate) ->
  StatAgendaView = Backbone.View.extend
    tagName: 'div'

    initialize: () ->
      _.bindAll(this, "render", "destroy")

    render: () ->
      allCount = 0
      practiseCount = 0
      learnedCount = 0
      for catID, category of @model.attributes
        allCount += category.all #model.get('counters').all
        practiseCount += category.onLearn #model.get('counters').new
        learnedCount += category.onRepeat #model.get('counters').practise

      params =
        allCount: allCount
        practiseCount: practiseCount
        learnedCount: learnedCount
        summ: allCount

      t = _.template(StatAgendaTemplate)
      @$el.html(t(params))
      return @$el

    destroy: () ->
      @$el.empty()
      @$el.remove()
      delete @$el

  StatAgendaView