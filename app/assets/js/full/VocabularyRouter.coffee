define [
  'underscore'
  'jquery'
  'tools/Mediator'

], (_, $, Mediator) ->
  VocabularyRouter = Backbone.Router.extend

    vocabularyController: null

    init: (vocabularyController) ->
      _.bindAll(this, "startTrain", "showCategories", "showLogin", "showLoad");
      @vocabularyController = vocabularyController

    routes:
      "train/:trainId"            : "startTrain"
      "categories"         : "showCategories"
      "categories/:catId"         : "showCategories"

      "login"                     : "showLogin"
      "load"                      : "showLoad"
      "book/:bookId"              : "showBook"

    showBook: (bookId) ->
      if (!bookId)
        bookId = "5188fc1894bc11230a87f866"
      @vocabularyController.showBook(bookId)

    startTrain: (trainId) ->
      @vocabularyController.startTrain(trainId)

    showCategories: (catId) ->
      if (!catId)
        @navigate("categories/all", {trigger: true})
      else
        @vocabularyController.showCategories(catId)

    showLogin:() ->
      @vocabularyController.showLogin()

    showLoad: () ->
      @vocabularyController.showLoadController()

  VocabularyRouter