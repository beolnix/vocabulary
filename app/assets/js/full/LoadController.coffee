define [
  'underscore'
  'jquery'

  'tools/Mediator'


  'text!/assets/js/full/loadTemplate.html'
], (_, $, Mediator, loadTemplate) ->
  class LoadController

    constructor: (@container) ->
      #

    getRoute: () ->
      "load"

    init: (el) ->
      @container.empty()
      t = _.template(loadTemplate)
      @container.html(t())


    destroy: () ->
      @container.empty()
