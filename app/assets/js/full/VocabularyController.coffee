define [
  'underscore'
  'jquery'
  'tools/Mediator'
  'settings/SettingsWidgetController'
  'LoginFormController'
  'categories/CategoryController'
  'train/TrainingController'
  'book/BookController'
  'LoadController'

], (_, $, Mediator, SettingsWidgetController, LoginFormController, CategoryController, TrainingController, BookController, LoadController) ->
  class VocabularyController
    contentContainer: null
    presentController: null
    loadController: null
    newController: null
    trainId: null
    router: null
    loginSuccess: false

    init: (router) ->
      _.bindAll(this, "showCategories", "successfulLogin", "destroyPresentController", "initNewController", "showCategories", "startDestroyAnimation");
      @container = $('#content_inn')
      @router = router
      @subscribe()
      SettingsWidgetController.init()
      if ( $('#loggedIn').val().toLowerCase() == "true" || @loginSuccess)
        SettingsWidgetController.showHeader()

    showCategories: (catId) ->
      @newController = new CategoryController(@container, @router, catId)
      @startDestroyAnimation()

    subscribe: () ->
      Mediator.subscribe('loginSuccess',  @successfulLogin)
      Mediator.subscribe('start_train', @startTrain, @)

    startTrain:(trainId) ->
      @newController = new TrainingController(@container, trainId)
      @startDestroyAnimation()

    showBook: (bookId) ->
      @newController = new BookController(@container, bookId)
      @startDestroyAnimation()

    showLoadController: () ->
      @newController = new LoadController(@container)
      @startDestroyAnimation()

    startDestroyAnimation: () ->
      animationParams =
        opacity: .1
      @container.animate(animationParams, "fast", @destroyPresentController )

    showLogin: () ->
      @newController = new LoginFormController(@container)
      @startDestroyAnimation()

    destroyPresentController: () ->
      if (@presentController != null)
        @presentController.destroy()
        delete @presentController
      @loadController = new LoadController(@container)
      @loadController.init()
      @initNewController()

    initNewController: () ->
      if ( $('#loggedIn').val().toLowerCase() == "true"  || @loginSuccess)
        @presentController = @newController
      else
        @presentController = new LoginFormController(@container)
      @router.navigate(@presentController.getRoute())
      @presentController.init()
      animationParams =
        opacity: 1
      @container.animate(animationParams, "fast")

    successfulLogin: () ->
      SettingsWidgetController.showHeader()
      @loginSuccess = true
      @newController = new CategoryController(@container, @router, "all")
      @startDestroyAnimation()

  VocabularyController