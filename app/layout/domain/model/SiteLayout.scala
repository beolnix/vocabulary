package layout


object SiteLayout extends Enumeration {
  type ExerciseType = Value
  val Mobile = Value
  val Full = Value
  val Test = Value
}
